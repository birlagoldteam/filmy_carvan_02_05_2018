<?php 
return [ 'EDUCATIONS' => array("SSC/10th" => "SSC/10th",'HSC/12th'=>'HSC/12th','Graduate'=>'Graduate','Post-Graduate'=>'Post-Graduate','Other'=>'Other'),

    'EMPLOYMENT'=> array('Working' =>'Working' , 'Not Working'=> 'Not Working','Retired'=>'Retired',
    	'Self Employment'=>'Self Employment','Business'=>'Business','Govt Service'=>'Govt Service',
    	'Pvt Service'=>'Pvt Service','Other'=>'Other'),





    'VEHICLE' =>  array('Two Wheeler' => 'Two Wheeler','Four Wheeler'=>'Four Wheeler','Heavy Vehicle'=>'Heavy Vehicle','Three Wheeler'=>'Three Wheeler','Other'=>'Other' ),


    'ANYLONGTERM' => array(' Allergy/anaphylaxis','Asthma',
    	'Atrial fibrillation','Amnesia',
    	'Chronic kidney disease','Anaemia','Angina'
    	,'Chronic pain','Depression',
    	'Anxiety and stress disorders',' Epilepsy',
    	'Diabetes','Hypertension','Blood disorders',
    	'Hepatitis B','Hepatitis C','HIV','Cancer',
    	'Migraine','Eczema','Obesity',
    	'Sleep disorders','Urinary Incontinence'),




    'LANDPROPERTY' => array('Residential' => 'Residential', 'Commercial'=>'Commercial','Agriculture'=>'Agriculture','Others'=>'Others'),

    
    'ExistingLoan' => array('Housing Loan' => 'Housing Loan', 'Personal Loan'=>'Personal Loan','Agricultural Loan'=>'Agricultural Loan','Study Loan'=>'Study Loan','Others'=>'Others'),




    'Insurance' =>  array('Life Insurance' =>'Life Insurance' , 'General Insurance'=>'General Insurance','Health Insurance'=>'Health Insurance','Others'=>'Others'),
     ];
?>
