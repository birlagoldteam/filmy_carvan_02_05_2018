@extends('layouts.app')
@section('page_title')
    Members
@endsection
@section('page_level_style_top')
<link rel="stylesheet" href="{{ URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('public/vendor/bootstrap-datepicker/bootstrap-datepicker3.css')}}" />
@endsection
@section('content')
<section class="page-header page-header-light page-header-more-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>My Account</h1>
				<ul class="breadcrumb breadcrumb-valign-mid">
					<li><a href="{{route('welcome')}}">Home</a></li>
					<li class="active">Member Registration</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<div class="container">
	<div class="agent-item">
		<div class="row">
			<!-- <div class="col-md-2">
				<img src="img/team/team-11.jpg" class="img-responsive" alt="">
			</div> -->
			<div class="col-md-6">
				<h3>{{Auth::user()->name}}</h3>
				@if(empty($membership))
				<h5 class="mt-xs mb-xs">Click <i class="fa fa-hand-o-down"></i> for Membership Registration </h5>
				<a class="btn btn-secondary btn-sm mt-md" href="#membership">Membership Registration</a>
				@else
				<h5 class="mt-xs mb-xs">Click <i class="fa fa-hand-o-down"></i> for Membership Updation </h5>
				<a class="btn btn-secondary btn-sm mt-md" href="{{route('membership-updation')}}">Update Membership</a>
				@endif
			</div>
			<div class="col-md-4">
				<ul class="list list-icons m-sm ml-xl">
					<li>
						<a href="mailto: mail@domain.com">
							<i class="icon-envelope-open icons"></i> {{Auth::user()->email}}
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-call-out icons"></i> {{Auth::user()->mobile}}
						</a>
					</li>
					<!-- <li>
						<a href="#">
							<i class="icon-social-linkedin icons"></i> Lindekin
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-social-facebook icons"></i> Facebook
						</a>
					</li> -->
				</ul>
			</div>
		</div>
	</div>
	@if(empty($membership))
	<div id="membership" class="heading heading-border heading-middle-border heading-middle-border-center">
		<h2>Filmy Caravan <strong>Membership Registration</strong></h2>
	</div>

	<!-- <div id="membership" class="row">
		<h2 align="center">Filmy Caravan Membership Registration</h2>
	</div> -->
	
	
	<div class="row">
		<div class="col-md-12">
			<div class="agent-item agent-item-detail">
				<div class="row">
					<div class="col-md-12">
						<form id="membershipForm" name="membershipForm" action="{{ url('membership-create') }}" method="POST">
							{{ csrf_field() }}
							<input type="hidden" value="Contact Form" name="subject" id="subject">
							<input type="hidden" name="id" id="id">
							{{$membership}}
							<div class="row">
								<h4>1. Personal Info.</h4>
								<div class="form-group">
									<div class="col-md-4">
										<label>Name *</label>
										<input type="text" value="{{Auth::user()->name}}" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" readonly>
									</div>
									<div class="col-md-4">
										<label>Email</label>
										<input type="email" value="{{Auth::user()->email}}" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" readonly>
									</div>
									<div class="col-md-4">
										<label>Mobile *</label>
										<input type="text" value="{{Auth::user()->mobile}}" data-msg-required="Please enter your Mobile." maxlength="100" class="form-control" name="mobile" id="mobile" readonly>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-4">
										<label>Gender</label>
										<select class="form-control mb-md" name="gender" id="gender">
											<option value="">---</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
											<option value="Transgender">Transgender</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Date of Birth</label>
											<input style="border-radius: 0px !important;" type="text" class="form-control datepicker"  name="date_of_birth" id="date_of_birth" readonly>
									</div>
									<div class="col-md-4">
										<label>Aadhar No. *</label>
										<input type="text" value="" data-msg-required="Please enter your Adhar No." maxlength="100" class="form-control" name="aadhar_number" id="aadhar">
									</div>
								</div>
							
								<div class="form-group">
									<div class="col-md-4">
										<label>Marital Status</label>
										<select class="form-control mb-md" name="marital_status" id="marital_status" name="marital_status">
											<option value="">---</option>
											<option value="Married">Married</option>
											<option value="Unmarried">Unmarried</option>
										</select>
									</div>
								</div>
							
								<h5>Address</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Country</label>
										<select class="form-control mb-md" name="country">
											<option value="1">India</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>State</label>
										<select class="form-control mb-md" name="state" id="state">
											<option value="">---</option>
											@foreach($state as $value)
												<option value="{{$value->id}}">{{$value->state_name}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-4">
										<label>City/District</label>
										<select class="form-control mb-md" name="city" id="city">
											<option value="">---</option>
										</select>
									</div>
								</div>
								<div class="form-group">	
									<div class="col-md-4">
										<label>Tehsil</label>
										<select class="form-control mb-md" name="tehsil" id="tehsil">
											<option value="">---</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Gram Panchayat</label>
										<select class="form-control mb-md" name="grampanchayat" id="grampanchayat">
											<option value="">---</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Village</label>
										<input type="text" value="" data-msg-required="Please enter your Village." class="form-control" name="village" id="village">
									</div>
								</div>
								<div class="form-group">	
									<div class="col-md-4">
										<label>Stree/Area</label>
										<input type="text" value="" data-msg-required="Please enter Street Name" maxlength="100" class="form-control" name="street_area" id="street_area">
									</div>
									<div class="col-md-4">
										<label>House Name/Number</label>
										<input type="text" value="" data-msg-required="Please enter your Home name/Number." class="form-control" name="house_name_no" id="house_name_no">
									</div>
									<div class="col-md-4">
										<label>Pincode</label>
										<input type="text" value="" data-msg-required="Please enter your Pincode." maxlength="100" class="form-control" name="pincode" id="pincode">
									</div>
								</div>
							
								<h5>Education</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Education</label>
										<select class="form-control mb-md" name="education" id="education">
											<option value="">---</option>
											<option value="SSC/10th">SSC/10th</option>
											<option value="HSC/12th">HSC/12th</option>
											<option value="Graduate">Graduate</option>
											<option value="Post-Graduate">Post-Graduate</option>
											<option value="Other">Other</option>
										</select>
									</div>
									<div class="col-md-4 other_education" style="display:none;">
										<label>Other Education</label>
										<input type="text" value="" data-msg-required="Please enter Other Education" maxlength="100" class="form-control" name="other_education" id="other_education">
									</div>
									<div class="col-md-4">
										<label>Language Known</label><br />
										<input type="hidden" value="" data-msg-required="Please enter your Languages you Know." class="form-control" name="language_known" id="language_known">
										<select class="form-control multiselect" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": true }' name="language" id="language">
											<option value="English">English</option>
											<option value="Hindi">Hindi</option>
											<option value="Regional">Regional</option>
										</select>
									</div>
								</div>
							
								<h5>Employment</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Employment</label>
										<select class="form-control mb-md" name="employment" id="employment">
											<option value="">---</option>
											<option value="Working">Working</option>
											<option value="Not Working">Not Working</option>
											<option value="Retired">Retired</option>
											<option value="Self Employment">Self Employment</option>
											<option value="Business">Business</option>
											<option value="Govt. Service">Govt. Service</option>
											<option value="Pvt. Service">Pvt. Service</option>
											<option value="Other">Other</option>
										</select>
									</div>
									<div class="col-md-4 employment_other" style="display:none;">
										<label>Employment Other</label>
										<input type="text" value="" data-msg-required="Others." maxlength="100" class="form-control" name="employment_other" id="employment_other">
									</div>
								</div>
							
								<h5>Health Profile</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Handicapped</label><br />
										<select class="form-control mb-md" name="handicapped" id="handicapped">
											<option value="">---</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Any Long-Term Diseases</label><br />
<!--
										<select class="form-control mb-md" name="long_term_diseases" id="long_term_diseases">
											<option value="">---</option>
											<option value="Polio">Polio</option>
											<option value="Tuberculosis">Tuberculosis</option>
											<option value="Other">Other</option>
											<option value="NA">NA</option>
										</select>
-->
										<input type="hidden" value="" data-msg-required="Please enter your Languages you Know." class="form-control" name="long_term_diseases" id="long_term_diseases">
										<select class="form-control multiselect long_term_diseases" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": true }'>
											<option value="Allergy/anaphylaxis">Allergy/anaphylaxis</option>
											<option value="Asthma">Asthma</option>
											<option value="Atrial fibrillation">Atrial fibrillation</option>
											<option value="Amnesia">Amnesia</option>
											<option value="Chronic kidney disease">Chronic kidney disease</option>
											<option value="Anaemia">Anaemia</option>
											<option value="Angina">Angina</option>
											<option value="Chronic pain">Chronic pain</option>
											<option value="Depression">Depression</option>
											<option value="Anxiety and stress disorders">Anxiety and stress disorders</option>
											<option value="Epilepsy">Epilepsy</option>
											<option value="Diabetes">Diabetes</option>
											<option value="Hypertension">Hypertension</option>
											<option value="Blood disorders">Blood disorders</option>
											<option value="Hepatitis B">Hepatitis B</option>
											<option value="Hepatitis C">Hepatitis C</option>
											<option value="HIV">HIV</option>
											<option value="Cancer">Cancer</option>
											<option value="Migraine">Migraine</option>
											<option value="Eczema">Eczema</option>
											<option value="Obesity">Obesity</option>
											<option value="Sleep disorders">Sleep disorders</option>
											<option value="Urinary Incontinence">Urinary Incontinence</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Social Needs for Me *</label><br />
										<select class="form-control multiselect user_needs" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": true }'>
										@foreach ($categories as $category)
										  <optgroup label="{{ $category->category_name }}">
										  @foreach ($category->subCategories as $need)
											 <option value="{{ $need->id }}">{{ $need->sub_category_name }}</option>
										  @endforeach
										  </optgroup>
										@endforeach
										</select>
										<input type="hidden" value="" class="form-control" id="social_needs_for_me" name="social_needs_for_me">
									</div>
								</div>
							
								<h4>2. My Family Info.</h4>
								<div class="form-group">
									<div class="col-md-12">
										<div class="table-responsive">
											<table class="table table-bordered table-striped table-condensed mb-none">
												<thead>
													<tr>
														<th width="5%">&nbsp;</th>
														<th width="20%">Name</th>
														<th width="5%">Age</th>
														<th width="20%">Relationship</th>
														<th width="10%">Occupation</th>
														<th width="20%">Mobile</th>
														<th width="20%">Our Social Needs</th>
													</tr>
												</thead>
												<tbody id="tbltable">
													
												</tbody>
												<tfoot>
													<tr>
														<td colspan="7"><button type="button" class="btn btn-default" onclick="add_row()">+</button></td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>	
								</div>
								
								
								<h4>3.  My Financial Status</h4>
								<div class="form-group">
									<div class="col-md-4">
										<label>Family Annual Income</label>
										<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="family_annual_income" id="family_annual_income">
									</div>
									<div class="col-md-4">
										<label>Monthly Expenses</label>
										<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="monthly_expenses" id="monthly_expenses">
									</div>
								</div>
							
								<div class="form-group">
									<div class="col-md-4">
										<label>Land/Property Ownership</label>
										<select class="form-control mb-md" name="property_ownership" id="property_ownership">
											<option value="">---</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
										</select>
									</div>
									<div class="col-md-4 proper property_type" style="display:none;">
										<label>Land/Property Type</label>
										<select class="form-control mb-md" name="property_type" id="property_type">
											<option value="">---</option>
											<option value="Residential">Residential</option>
											<option value="Commercial">Commercial</option>
											<option value="Agriculture">Agriculture</option>
											<option value="Others">Others</option>
										</select>
									</div>
									<div class="col-md-4 commercial" style="display:none;">
										<label>Commercial</label>
										<select class="form-control mb-md" name="commercial" id="commercial">
											<option value="">---</option>
											<option value="Office">Office</option>
											<option value="Commercial">Shop</option>
										</select>
									</div>
									<div class="col-md-4 other_property" style="display:none;">
										<label>Other Property</label>
										<input type="text" value="" data-msg-required="Please enter Street Name" maxlength="100" class="form-control" name="other_property" id="other_property">
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-md-4">
										<label>Vehicle</label>
										<select class="form-control mb-md" name="vehicle" id="vehicle">
											<option>---</option>
											<option value="Two Wheeler">Two Wheeler</option>
											<option value="Two Wheeler">Three Wheeler</option>
											<option value="Four Wheeler">Four Wheeler</option>
											<option value="Heavy Vehicle">Heavy Vehicle</option>
											<option value="Other">Other</option>
										</select>
									</div>
									<div class="col-md-4">
										<label style="display:none;" class="other_vehicle">Other Vehicle</label>
										<input style="display:none;" type="text" value="" data-msg-required="Please enter Street Name" maxlength="100" class="form-control other_vehicle" name="other_vehicle" id="other_vehicle">
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-md-4">
										<label>Existing Loan</label>
										<select class="form-control mb-md" name="loan" id="loan">
											<option>---</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
										</select>
									</div>
									<div class="col-md-4 loan_type" style="display:none;">
										<label>Loan Type</label>
										<select class="form-control mb-md" name="loan_type" id="loan_type">
											<option value="Housing Loan">Housing Loan</option>
											<option value="Personal Loan">Personal Loan</option>
											<option value="Agricultural Loan">Agricultural Loan</option>
											<option value="Study Loan">Study Loan</option>
											<option value="Others">Others</option>
										</select>
									</div>
									<div class="col-md-4 other_loan" style="display:none;">
										<label>Other Loan</label>
										<input type="text" value="" data-msg-required="Please enter Other Loan" maxlength="100" class="form-control" name="other_loan" id="other_loan">
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-md-4">
										<label>Insurance</label>
										<select class="form-control mb-md" name="insurance" id="insurance">
											<option>---</option>
											<option value="Yes">Yes</option>
											<option value="No">No</option>
										</select>
									</div>
									<div class="col-md-4 insurance_type" style="display:none;">
										<label>Insurance</label>
										<select class="form-control mb-md" name="insurance_type" id="insurance_type">
											<option value="Life Insurance">Life Insurance</option>
											<option value="General Insurance">General Insurance</option>
											<option value="Health Insurance">Health Insurance</option>
											<option value="Others">Others</option>
										</select>
									</div>
									<div class="col-md-4 other_insurance" style="display:none;">
										<label>Other Insurance</label>
										<input type="text" value="" data-msg-required="Please enter Other Loan" maxlength="100" class="form-control" name="other_insurance" id="other_insurance">
									</div>
									
								</div>
								<div class="form-group insurance_type" style="display:none;">
									<div class="col-md-4">
										<label>Insured Company Name</label>
										<input type="text" value="" data-msg-required="Please enter Insured Name" maxlength="100" class="form-control" name="insured_company_name" id="insured_company_name">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label class="checkbox-inline">
											<input type="checkbox" id="inlineCheckbox2" name="agree" value="option2">
											I Agree to <a href="" data-toggle="modal" data-target="#memberreg"><u>Terms & Conditions</u></a>
										</label>												
									</div>
								</div>
								<h5>Declaration</h5>
								<div class="form-group">
									<div class="col-md-12">
										<div class="radio-list" data-error-container="#form_2_membership_error">
											<label class="checkbox-inline">
												<input type="checkbox" id="inlineCheckbox2" name="declaration" value="option2"> K Sera Sera (also known as KSS Ltd.) was incorporated in September 1995, is headquartered in Mumbai and is a publicly listed company, listed on the BSE & NSE. As a major player in Indian Cinema, KSS has produced and distributed over 100 films, including some box-office blockbusters like Golmaal, Partner, Sarkar , Sarkar Raj, Ab Tak Chappan & many more. KSS is headed by a team of professionals and technocrats, whose collective experience forms the heart of this organization. KSS, as it is commonly known, focuses on the entertainment film business with three business entities focusing on digital cinema rollout, the building of Miniplex theater screens, and alternative content programming in movie theater, respectively.
											</label>
											<div id="form_2_membership_error"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Register" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
</div>

	<div class="modal fade" id="memberreg" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="largeModalLabel">Terms & Conditions & PRIVACY AND SECURITY POLICY</h4>
				</div>
				<div class="modal-body">
					<p align="justify">
					<p align="justify">Welcome to FILMY CARAVAN Membership Registration (Application name & Website/Mobile App/E Registration Form Address) owned and operated by K Sera Sera Box Office P Limited (KSS Box Office/Filmy Caravan), a company incorporated under the provisions of the Companies Act, 1956 and having its registered office at 101, A & 102, 1st Floor, Morya Landmark II, Andheri West, Mumbai 400053.</p>
					<p align="justify">This document/agreement/understanding is an electronic record in terms of Information Technology Act, 2000 and is generated by a computer system and does not require any physical or digital signatures. This document is published in accordance with the provisions of Rule 3 of the Information Technology (Intermediaries Guidelines) Rules, 2011.</p>
					<p align="justify">Before you may use the Website/Mobile App/E Registration Form, you must read all of the terms and conditions (“Terms”) herein and the Privacy Policy provided on the Website/Mobile App/E Registration Form. By using KSS Box Office/Filmy Caravan's products, software, services and Website/Mobile App/E Registration Form (“Services”), you understand and agree that KSS Box Office/Filmy Caravan will treat your use of the Services as acceptance of these terms from such point of usage. You may not use the Services if you do not accept the Terms. If you do not agree to be bound by these Terms and the Privacy Policy, you may not use the Website/Mobile App/E Registration Form in any way. It is strongly recommended for you to return to this page periodically to review the most current version of the Terms. KSS Box Office/Filmy Caravan reserves the right at any time, at its sole discretion, to change or otherwise modify the Terms without prior notice, and your continued access or use of this Website/Mobile App/E Registration Form signifies your acceptance of the updated or modified Terms. If you object to these Terms or any subsequent modifications to these Terms or become dissatisfied with the Website/Mobile App/E Registration Form in any way, your only recourse is to immediately terminate use of the Website/Mobile App/E Registration Form.</p>

					<h4>1.  Proprietary rights</h4>
					<p align="justify">You acknowledge and agree that KSS Box Office/Filmy Caravan owns all rights, title and interest in and to the Services, including any intellectual property rights which subsist in the Services (whether registered or not). You further acknowledge that the Services may contain information which is designated confidential by KSS Box Office/Filmy Caravan and that you shall not disclose such information without KSS Box Office/Filmy Caravan's prior written consent.</p>
					<p align="justify">KSS Box Office/Filmy Caravan grants you a limited license to access and make personal use of the Website/Mobile App/E Registration Form and the Services. This license does not confer any right to download, copy, create a derivative work from, modify, reverse engineer, reverse assemble or otherwise attempt to discover any source code, sell, assign, sub-license, grant a security interest in or otherwise transfer any right in the Services. You do not have the right to use any of KSS Box Office/Filmy Caravan's trade names, trademarks, service marks, logos, domain names, and other distinctive brand features. You do not have the right to remove, obscure, or alter any proprietary rights notices (including trademark and copyright notices), which may be affixed to or contained within the Services. You will not copy or transmit any of the Services.</p>
					<h4>2. Usage of the Website/Mobile App/E Registration Form</h4>
					<p align="justify">The Website/Mobile App/E Registration Form is intended for personal / Family level Need Analysis and commercial, non-commercial use intended for Social Need Analysis as well Social Development Projects. Only register to become a member of the Website/Mobile App/E Registration Form if you are above the age of 18 and can enter into binding contracts. You are responsible for maintaining the secrecy of your passwords, login and account information. You will be responsible for all use of the Website/Mobile App/E Registration Form by you and anyone using your password and login information (with or without your permission).</p>
					<p align="justify">You also agree to provide true, accurate, current and complete information about yourself as prompted by the Website/Mobile App/E Registration Form. If you provide any information that is untrue, inaccurate, not updated or incomplete (or becomes untrue, inaccurate, not updated or incomplete), or KSS Box Office/Filmy Caravan has reasonable grounds to suspect that such information is untrue, inaccurate, not updated or incomplete, KSS Box Office/Filmy Caravan has the right to suspend or terminate your account and refuse any and all current or future use of the Website/Mobile App/E Registration Form (or any portion thereof) or Services in connection thereto.</p>
					<p align="justify">By making use of the Website/Mobile App/E Registration Form, and furnishing your personal/contact details, you hereby agree that you are interested in availing and purchasing the Services that you have selected. You hereby agree that KSS Box Office/Filmy Caravan may contact you either electronically or through phone, to understand your interest in the selected products and Services and to fulfil your demand or complete your application. You specifically understand and agree that by using the Website/Mobile App/E Registration Form you authorize KSS Box Office/Filmy Caravan and its affiliates and partners to contact you for any follow up calls in relation to the Services provided through the Website/Mobile App/E Registration Form. You expressly waive the Do Not Call registrations on your phone/mobile numbers for contacting you for this purpose. You also agree that KSS Box Office/Filmy Caravan reserves the right to make your details available to partners and you may be contacted by the partners for information and for sales through email, telephone and/or sms. You agree to receive promotional materials and/or special offers from KSS Box Office/Filmy Caravan through emails or sms.</p>
					<p align="justify">The usage of the Website/Mobile App/E Registration Form may also require you to provide consent for keying in your Sensitive Information (“SI”) (including but not limited to user ids and passwords), as may be necessary to process your application through this Website/Mobile App/E Registration Form. SI keyed in shall be required for enabling hassle free, faster and paperless (to the extent possible) processing of applications for financial products so opted / availed by you. KSS Box Office/Filmy Caravan shall adhere to best industry practices including information security, data protection and privacy law while processing such applications and the SI interface where key in of SI is required shall not be stored. However, KSS Box Office/Filmy Caravan shall not be liable to you against any liability or claims which may arise out of such transactions.</p>
					<p align="justify">You may only use the Website/Mobile App/E Registration Form to search for and to apply for loans, mutual funds, credit cards or other financial products as may be displayed on the Website/Mobile App/E Registration Form from time to time and you shall not use the Website/Mobile App/E Registration Form to make any fraudulent applications. You agree not to use the Website/Mobile App/E Registration Form for any purpose that is unlawful, illegal or forbidden by these Terms, or any local laws that might apply to you. Since the Website/Mobile App/E Registration Form is in operation in India, while using the Website/Mobile App/E Registration Form, you shall agree to comply with laws that apply to India and your own country (in case of you being a foreign national). We may, at our sole discretion, at any time and without advance notice or liability, suspend, terminate or restrict your access to all or any component of the Website/Mobile App/E Registration Form.</p>
					<p align="justify">You are prohibited from posting or transmitting to or through this Website/Mobile App/E Registration Form: (i) any unlawful, threatening, libelous, defamatory, obscene, pornographic, or other material or content that would violate rights of publicity and/or privacy or that would violate any law; (ii) any commercial material or content (including, but not limited to, solicitation of funds, advertising, or marketing of any good or services); and (iii) any material or content that infringes, misappropriates or violates any copyright, trademark, patent right or other proprietary right of any third party. You shall be solely liable for any damages resulting from any violation of the foregoing restrictions, or any other harm resulting from your posting of content to this Website/Mobile App/E Registration Form.</p>
					<h4>3. Privacy policy</h4>
					<p align="justify">By using the Website/Mobile App/E Registration Form, you hereby consent to the use of your information as we have outlined in our Privacy Policy.</p>
					<h4>4. Our partners</h4>
					<p align="justify">Display of loan, mutual funds, credit card or other financial products, offered by third parties, on the Website/Mobile App/E Registration Form does not in any way imply, suggest, or constitute any sponsorship, recommendation, opinion, advice or approval of KSS Box Office/Filmy Caravan against such third parties or their products. You agree that KSS Box Office/Filmy Caravan is in no way responsible for the accuracy, timeliness or completeness of information it may obtain from these third parties. Your interaction with any third party accessed through the Website/Mobile App/E Registration Form is at your own risk, and KSS Box Office/Filmy Caravan will have no liability with respect to the acts, omissions, errors, representations, warranties, breaches or negligence of any such third parties or for any personal injuries, death, property damage, or other damages or expenses resulting from your interactions with the third parties.</p>
					<p align="justify">You agree and acknowledge that the credit shall be at the sole discretion of KSS Box Office/Filmy Caravan’s financial partners (lenders, credit card companies, mutual fund companies) while making any application through the Website/Mobile App/E Registration Form for a financial product offered by such financial partners; KSS Box Office/Filmy Caravan shall not be held liable for any delay, rejection or approval of any application made through its Website/Mobile App/E Registration Form.</p>
					<h4>5.  Disclaimer of warranty</h4>
					<p align="justify">The Website/Mobile App/E Registration Form and all content and Services provided on the Website/Mobile App/E Registration Form are provided on an "as is" and "as available" basis. KSS Box Office/Filmy Caravan expressly disclaims all warranties of any kind, whether express or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose, title, non-infringement, and security and accuracy, as well as all warranties arising by usage of trade, course of dealing, or course of performance. KSS Box Office/Filmy Caravan makes no warranty, and expressly disclaims any obligation, that: (a) the content will be up-to-date, complete, comprehensive, accurate or applicable to your circumstances; (b) The Website/Mobile App/E Registration Form will meet your requirements or will be available on an uninterrupted, timely, secure, or error-free basis; (c) the results that may be obtained from the use of the Website/Mobile App/E Registration Form or any Services offered through the Website/Mobile App/E Registration Form will be accurate or reliable; or (d) the quality of any products, services, information, or other material obtained by you through the Website/Mobile App/E Registration Form will meet your expectations.</p>
					<h4>6. Limitation of liability</h4>
					<p align="justify">KSS Box Office/Filmy Caravan (including its officers, directors, employees, representatives, affiliates, and providers) will not be responsible or liable for (a) any injury, death, loss, claim, act of god, accident, delay, or any direct, special, exemplary, punitive, indirect, incidental or consequential damages of any kind (including without limitation lost profits or lost savings), whether based in contract, tort, strict liability or otherwise, that arise out of or is in any way connected with (i) any failure or delay (including without limitation the use of or inability to use any component of the Website/Mobile App/E Registration Form), or (ii) any use of the Website/Mobile App/E Registration Form or content, or (iii) the performance or non-performance by us or any provider, even if we have been advised of the possibility of damages to such parties or any other party, or (b) any damages to or viruses that may infect your computer equipment or other property as the result of your access to the Website/Mobile App/E Registration Form or your downloading of any content from the Website/Mobile App/E Registration Form.</p>
					<p align="justify">The Website/Mobile App/E Registration Form may provide links to other third party Website/Mobile App/E Registration Forms. However, since KSS Box Office/Filmy Caravan has no control over such third party Website/Mobile App/E Registration Forms, you acknowledge and agree that KSS Box Office/Filmy Caravan is not responsible for the availability of such third party Website/Mobile App/E Registration Forms, and does not endorse and is not responsible or liable for any content, advertising, products or other materials on or available from such third party Website/Mobile App/E Registration Forms. You further acknowledge and agree that KSS Box Office/Filmy Caravan shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party Website/Mobile App/E Registration Forms.</p>
					<h4>7. Indemnity</h4>
					<p align="justify">You agree to indemnify and hold KSS Box Office/Filmy Caravan (and its officers, directors, agents and employees) harmless from any and against any claims, causes of action, demands, recoveries, losses, damages, fines, penalties or other costs or expenses of any kind or nature, including reasonable attorneys' fees, or arising out of or related to your breach of this Terms, your violation of any law or the rights of a third party, or your use of the Website/Mobile App/E Registration Form.</p>
					<h4>8. Additional terms</h4>
					<p align="justify">You may not assign or otherwise transfer your rights or obligations under these Terms. KSS Box Office/Filmy Caravan may assign its rights and duties under these Terms without any such assignment being considered a change to the Terms and without any notice to you. If we fail to act on your breach or anyone else's breach on any occasion, we are not waiving our right to act with respect to future or similar breaches. Other terms and conditions may apply to loans, mutual funds, credit cards or other financial products that you may apply on the Website/Mobile App/E Registration Form. You will observe these other terms and conditions. The laws of the India, without regard to its conflict of laws rules, will govern these Terms, as well as your and our observance of the same. If you take any legal action relating to your use of the Website/Mobile App/E Registration Form or these Terms, you agree to file such action only in the courts located in Chennai, India. In any such action that we may initiate, the prevailing party will be entitled to recover all legal expenses incurred in connection with the legal action, including but not limited to costs, both taxable and non-taxable, and reasonable attorney fees. You acknowledge that you have read and have understood these Terms, and that these Terms have the same force and effect as a signed agreement.</p>                     
					<h3>PRIVACY AND SECURITY POLICY</h3>

					<h4>1. INTRODUCTION:</h4>
					<p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan(hereinafter referred to as “K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan/ we/ our”) acknowledges the expectations of its customers (hereinafter referred to as “customer/ you/ your”) regarding privacy, confidentiality and security of Personal Information that resides with the K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan. Keeping Personal Information of customers secure and preventing any misuse thereof is a top priority of K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.</p>
					<p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan is strongly committed to protect the privacy of its customers and has taken all necessary and reasonable measures in line with the best industry practice to protect the confidentiality of the customer information and its transmission through Website/Mobile App/E Registration Form and it shall not be held liable for disclosure of the confidential information when in accordance with this privacy Commitment or in terms of the ‘Terms of Use’ of Website/Mobile App/E Registration Form or agreement, if any, with the customers.</p>
					<p align="justify">This Privacy & Security Policy (hereinafter referred to as “Policy”) explains how we protect Personal Information provided on our Website/Mobile App/E Registration Form www.K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com (hereinafter referred to as “Website/Mobile App/E Registration Form”) and how we use that information in connection with our service offered through the Website/Mobile App/E Registration Form (hereinafter called as "Service").</p>
					<h4>2. PERSONAL INFORMATION:</h4>
					<p align="justify">Personal Information (which may be a Sensitive Information “SI”) means any information /documents/details that relates to a natural person, which either directly or indirectly, in combination with other information available or likely to be available with the K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan, can identify such person.</p>
					<h4>3. APPLICABILITY:</h4>
					<p align="justify">This Policy is applicable to the Personal Information (including but not limited to details pertaining to your name, parentage, marital status, nationality, state of residence, city of residence, email address, date of birth, gender, contact number/mobile number, user ids, passwords, recent photograph, signature image, e-KYC through UIDAI, Aadhaar based e-Signature and other Know Your Customer (KYC) documents like address proof, identity proof, income proof, PAN or such other officially valid documents/details (OVDs) accepted for concluding a financial transaction) which are shared/ uploaded by you, as and when you avail the products and services vide the online application forms and questionnaires through usage of Website/Mobile App/E Registration Form and which K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan may become privy to. By visiting and/or using our Website/Mobile App/E Registration Form, you agree to this Policy. Further, this policy applies to all current and former visitors to the Website/Mobile App/E Registration Form and to our online customers. It is strongly recommended for you to return to this page periodically to review the most current version of the Policy. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan reserves the right at any time, at its sole discretion, to change or otherwise modify the Policy without prior notice, and your continued access or use of this Website/Mobile App/E Registration Form signifies your acceptance of the updated or modified Policy.</p> 
					<p align="justify">However, if we make any material change to the Policy, we will notify you by email (sent to the e-mail address specified in your account) or by means of a notice on this Website/Mobile App/E Registration Form prior to the change becoming effective.</p>
					<h4>4. EXPRESS CONSENT:</h4>
					<ul>
						<li>You while providing the details/documents over the Website/Mobile App/E Registration Form, including but not limited to Personal Information as mentioned hereinabove, expressly consent to K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan (its affiliates and business partners) to contact you, to make follow up calls in relation to the services provided through the Website/Mobile App/E Registration Form, for imparting product knowledge, offering promotional offers running on Website/Mobile App/E Registration Form & various other offers offered by its partners or Group companies on the Sites.</li>
						<li>The usage of the Website/Mobile App/E Registration Form may also require you to provide consent for keying in/ uploading your Personal Information and Sensitive Information (including but not limited to user ids and passwords), as may be necessary to process your application through the Website/Mobile App/E Registration Form. Sensitive information keyed in/ uploaded shall be required for enabling hassle free, faster and paperless (to the extent possible) processing of applications for financial products so opted / availed by you.</li>
						<li>You hereby authorize and expressly consent us to share your demographic and Personal Information with third parties including but not limited to Credit Information Companies to do an aggregate check of your credit profile and for K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan to send you targeted communications and offers by agreeing to the Terms of Use while accessing the Website/Mobile App/E Registration Form. You hereby also consent K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan to procure credit information and indicative scores from the credit information companies on your behalf.</li>
						<li>If you are no longer interested in sharing your Personal and Sensitive Information, please e-mail your request at: privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com. Please note that it may take about 72 business hours to process your request. In furtherance to your usage of the Website/Mobile App/E Registration Form, you expressly waive the Do Not Call (DNC) / Do Not Disturb (DND) registrations on your phone/mobile numbers for contacting you for this purpose. Hence, there is no DNC / DND check required for the number you have left on our Website/Mobile App/E Registration Form. Such modes of contacting you may include sending SMSs, email alerts and / or telephonic calls.</li>
						<li>K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan reserves the right (and you expressly authorize K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan) to share or disclose your Personal Information/ Sensitive Information when K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan determines, in its sole discretion, that the disclosure of such information is necessary or appropriate under the law for the time being in force.</li>
					</ul>
					<h4>5. OPT-OUT:</h4>
					<p align="justify">In case you do not want to be disturbed over telephonic calls, kindly fill up the details requested under this section below, includingthe details of the telephone number(s) on which you do not wish to be contacted and submit the same at privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com from your email address registered at K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan. The details that you provide through the opt-out email will remain confidential and once you have submitted the same to us, your telephone number(s) will be removed from all our telemarketing calling lists within 15 working days. We will make every effort to ensure that you do not get any further telemarketing calls on such telephone number(s). The details to be submitted for opting-out shall be as below:
					<ul>
						<li>Title*</li>   
						<li>First Name*</li>  
						<li>Last Name*</li>   
						<li>Country*</li>     
						<li>State*</li>   
						<li>City*</li>    
						<li>Email</li>    
						<li>Mobile Number #</li>  
						<li>Landline Phone Number #</li>
					</ul>
					</p>  
					<p align="justify">* Mandatory fields<br/>
					# Please enter your 10-digit mobile number. For International Numbers, please do not enter your country code</p>
					<h4>6.  PURPOSE AND USAGE:</h4>
					<p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan will not sell or rent your Personal Information to anyone, for any reason, at any time. However, we will be sharing your Personal Information/ Sensitive Information with our affiliates and business partners including credit information companies, where we feel that you will be assisted better for the purpose of underwriting and approval of your credit card, loan and approving investment in Mutual Fund or any other financial product transaction/related transaction or for doing an aggregate check of your credit profile on your behalf and ending you targeted communications and offers. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan uses the information collected and appropriately notifies you to manage its business and offer an enhanced, personalized online experience to you on its Website/Mobile App/E Registration Form. By registering on the Website/Mobile App/E Registration Form, you authorize K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan to send texts and email alerts to you and any other service requirements, including promotional mails and SMSs Further, it enables K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan to:
						<ul>
							<li>analyse usage of the Website/Mobile App/E Registration Form and to improve the Service;</li>
							<li>send you any administrative notices, offer alerts and communications relevant to your use of the Service;</li>
							<li>enable you to apply for certain products and services for which you have chosen to apply;</li>
							<li>Carry market research, project planning, troubleshooting issues, detecting and protecting against error, fraud or other criminal activity;</li>
							<li>bind third-party contractors that provide services to K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan and are bound by these same privacy restrictions;</li>
							<li>Comply with all applicable laws and regulations;</li>
							<li>enforce K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan Terms of Use.</li>
						</ul>
					</p>
					<p align="justify">In the event that you access the Service as brought to you by one of our partners either through the Website/Mobile App/E Registration Form (www.K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com) or on being redirected from a co-branded URL otherwise referred to as a white-label site, your name, e-mail address, mobile number, date of birth, employment type, residency status, income details/proofs, Form 26 AS, PAN, details of loan / credit card/ Mutual Fund applied for and loan, credit card and Mutual Fund status or any other financial product status as may be provided to that partner when your application is submitted and whenever the status of application is updated. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan has a business relationship with these partners and you may not opt-out of sharing your information with these partners if you have applied via a co-branded URL or directly through the Website/Mobile App/E Registration Form as the case may be.</p>
					<p align="justify">For availing the Service such as applying for a loan, credit card, Mutual Fund or any other financial product we will require you to provide/upload on the Website/Mobile App/E Registration Form the details such as a your name, parentage, marital status, email address, nationality, location, mobile number, PAN, employment & income details/proofs, Form 26 AS, recent photograph, signature image, other Know Your Customer (KYC) documents like address proof, identity proof and personally identifying information about a potential co-loan applicant (should you select this option), and to participate on our forum boards, a username (collectively the "Registration Information").</p>
					<p align="justify">You may opt out of location based services at any time by editing the setting of your browser.</p>
					<p align="justify">In order to provide your bank statement or pay slip electronically along with your loan application, you also must provide your third-party account credentials ("Account Credentials") to allow K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan to retrieve your account data at those other financial institutions ("Account Information") for your use. Your Account Credentials are only used once to retrieve your bank statements/pay slips, Form 26 AS and are not stored in our system. However, K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan shall not be liable to you against any liability or claims which may arise out of such transactions being carried on your own accord.</p>
					<p align="justify">We may also use third party service providers to provide the Service to you, such as sending e-mail messages on our behalf or hosting and operating a particular feature or functionality of the Service. Our contracts with these third parties outline the appropriate use and handling of your information and prohibit them from using any of your Personal Information for purposes unrelated to the product or service they're providing. We require such third parties to maintain the confidentiality of the information provide to them.</p>
					<h4>7. DISCLOSURE / SHARING:</h4>
					<p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan does not disclose sensitive personal data or Personal Information including SI of a customer except as directed by law or as per mandate received from the customer / applicant. Upon your written request K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan will provide you with information on whether we hold any of your personal information. No specific information about customer accounts or other personally identifiable data is shared with non-affiliated third parties unless any of the following conditions is met:
						<ul>
							<li>To help complete a transaction initiated by you</li>
							<li>To perform support services through a third party service provider, provided it conforms to the Privacy Policy of the K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan and your prior consent to do so is obtained</li>
							<li>You have specifically authorized it</li>
							<li>The disclosure is necessary for compliance of a legal obligation or as required by law, such as to comply with a subpoena, or similar legal process</li>
							<li>The information is shared with Government agencies mandated under law</li>
							<li>The information is shared with any third party by an order under the law</li>
							<li>When we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request.</li>
						</ul>
					</p>
					<p align="justify">There are number of products/services such as loans, credit cards, mutual funds, offered by third Parties on the Website/Mobile App/E Registration Form, such as lenders, banks, credit card issuers. If you choose to apply for these separate products or services, disclose information to these providers, then their use of your information is governed by their privacy policies. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan is not responsible for their privacy policies.</p>
					<h4>8.  INTIMATION BY CUSTOMERS REGARDING CHANGE IN REGISTRATION INFORMATION:</h4>
					<p align="justify">If your Registration Information provided to us when you had applied for a product on our Website/Mobile App/E Registration Form or Sites changes, you may update it whenever you apply for a new product via our Website/Mobile App/E Registration Form or Sites. To review and update your Personal Information and to ensure that the same is accurate while your application is in process, you may contact us at support@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com. You will not be able to update the information you have provided in an application after a decision has already been made on it; however, you may create and submit a new application with your updated information.</p>
					<p align="justify">Note: We will retain your information for as long as your account is active or as needed to provide you services. If you wish to cancel your account or request that we no longer use your information to provide you services, contact us at support@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com. We will respond to your request within a reasonable timeframe. However, we will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and to enforce our agreements.</p>
					<h4>9.  EMAIL & SMS COMMUNICATIONS FROM US AND OUR PARTNERS:</h4>
					<p align="justify">We provide our registered customers with periodic emailers and email/SMS alerts. We also allow users to subscribe to email newsletters and from time to time may transmit emails promoting K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan or third-party products. Subject to the express consent clause above, K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan’s Website/Mobile App/E Registration Form subscribers may opt-out of receiving our promotional emails and terminate their newsletter subscriptions by following the instructions in the emails. Opting out in this manner will not end transmission of service-related emails/SMS, such as email/SMS alerts. The above services are also provided by our partners.</p>
					<h4>10. LOG FILES:</h4>
					<p align="justify">This information may include internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and/or clickstream data. We may use the collected log information about you to improve services offered to you, to improve marketing, analytics, or Website/Mobile App/E Registration Form functionality.</p>
					<h4>11. TRACKING TECHNOLOGIES:</h4>
					<p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan and its partners use cookies or similar technologies to analyze trends, administer the Website/Mobile App/E Registration Form, track users’ movements around the Website/Mobile App/E Registration Form, and to gather demographic information about our user base as a whole. You can control the use of cookies at the individual browser level, but if you choose to disable cookies, it may limit your use of certain features or functions on our Website/Mobile App/E Registration Form or service. To manage Flash cookies, please visit the flash player support page.</p>
					<h4>12. BEHAVIOURAL TARGETING / RE-TARGETING:</h4>
					<p align="justify">We partner with a third party service provider to either display advertising on our Website/Mobile App/E Registration Form or to manage our advertising on other sites. Our third party partner may use technologies such as cookies to gather information about your activities on this Website/Mobile App/E Registration Form and other Sites in order to provide you advertising based upon your browsing activities and interests. If you wish to not have this information used for the purpose of serving you interest-based ads, you may opt-out by clicking here. Please note this does not opt you out of being served ads. You will continue to receive generic ads.</p>
					<h4>13. THIRD PARTIES WILL NOT BE GIVEN YOUR PERSONAL INFORMATION UNLESS YOU DIRECT K SERA SERA BOX OFFCE PVT LTD/KSS BOX OFFICE/FILMY CARAVAN TO DO SO:</h4>
					<p align="justify">There are several products and services, such as loans, credit cards, Mutual Fund, Gold Ornaments, Gold SIP and other financial products being offered by third parties on our Website/Mobile App/E Registration Form, such as lenders, banks, credit card issuers and mutual funds (collectively "Offers"). If you choose to apply for these separate products or services, disclose information to the providers, or grant them permission to collect information about you, then their use of your information is governed by their privacy policies. You should evaluate the practices of these external services providers before deciding to use their services. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan is not responsible for their privacy practices.</p>
					<h4>14. TESTIMONIALS, BLOGS AND OTHER FORUMS ON K SERA SERA BOX OFFCE PVT LTD/KSS BOX OFFICE/FILMY CARAVAN WEBSITE/MOBILE APP/E REGISTRATION FORM:</h4>
					<p align="justify">With your consent K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan may post your testimonial along with your name. If you want your testimonial removed, please contact K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan at support@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com.</p>
					<p align="justify">If you use a blog or other public forum on Website/Mobile App/E Registration Form, any information you submit there can be read, collected or used by other users and could be used to send you unsolicited messages. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan is not responsible for the Personal Information you choose to submit in these forums.</p>
					<h4>15. ADDITIONAL POLICY INFORMATION:</h4>
					<p align="justify">Widgets: Our Website/Mobile App/E Registration Form includes Widgets, which are interactive mini-programs that run on our Website/Mobile App/E Registration Form to provide specific services from another company (e.g. displaying the news, opinions, music, etc). Personal Information, such as your email address, may be collected through the Widget. Cookies may also be set by the Widget to enable it to function properly. Information collected by this Widget is governed by the privacy policy of the company that created it and not by the K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.</p>
					<p align="justify">Single Sign-On: You can log in to our Website/Mobile App/E Registration Form using sign-in services such as Facebook Connect or an Open ID provider. These services will authenticate your identity and provide you the option to share certain Personal Information with us such as your sign-in information, name and email address to link between the sites. Social networking media services like Facebook & Twitter give you the option to post information about your activities on this Website/Mobile App/E Registration Form to your profile page to share with others within your network.</p>
					<p align="justify">Like Button: If you use the "Like" button to share something that item will appear on your Facebook profile page and also on your friends’ newsfeed depending on your Facebook privacy settings. You may also receive updates in your Facebook newsfeed from this Website/Mobile App/E Registration Form or item in the future. Facebook also collects information such as which pages you have visited on this and other sites that have implemented the "Like" button.</p>
					<p align="justify">Links to 3rd Party Sites: Our Website/Mobile App/E Registration Form includes links to other Website/Mobile App/E Registration Forms whose privacy practices may differ from those of K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan. If you submit your Personal Information to any of those sites, your information is governed by their privacy policies. We encourage you to carefully read the privacy policy of any Website/Mobile App/E Registration Form you visit.</p>
					<h4>16. WE KEEP YOUR DATA SECURE:</h4>
					<p align="justify">We follow generally accepted standards to protect the Personal Information submitted to us, both during transmission and once we receive it. Since no method of transmission over the Internet, or method of electronic storage, is 100% secure, therefore, we cannot guarantee its absolute security. If you have any questions about security on our Website/Mobile App/E Registration Form, you can contact us at privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com.</p>
					<p align="justify">We use a combination of firewalls, encryption techniques and authentication procedures, among others, to maintain the security of your online session and to protect K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan online accounts and systems from unauthorized access.</p>
					<p align="justify">When you register for the Service, K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan requires a password from you for your privacy and security. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan transmits information such as your Registration Information for Website/Mobile App/E Registration Form or Account Credentials securely.</p>
					<p align="justify">Our servers are in secure facilities where access requires multiple levels of authentication, including an identity card and biometrics recognition procedures. Security personnel monitor the facilities 7 days a week, 24 hours a day.</p>
					<p align="justify">Our databases are protected from general employee access both physically and logically. We encrypt your Service password so that your password cannot be recovered, even by us. All backup drives and tapes also are encrypted.</p>
					<p align="justify">We enforce physical access controls to our buildings.</p>
					<p align="justify">No employee may put any sensitive content on any insecure machine (i.e., nothing can be taken from the database and put on an insecure laptop).</p>
					<p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan has been verified for its use of SSL encryption technologies. In addition, K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan tests the Website/Mobile App/E Registration Form using Acunetix scan for any failure points that would allow hacking.</p>
					<p align="justify">However, it is important to understand that these precautions apply only to our Website/Mobile App/E Registration Form and systems.</p>
					<h4>17. ALL PRIVATE INFORMATION IS ENCRYPTED AND COMMUNICATED SECURELY:</h4>
					<p align="justify">All communications between your computer and our Website/Mobile App/E Registration Form that contain any Personal Information are encrypted. This enables client and server applications to communicate in a way that is designed to prevent eavesdropping, tampering and message forgery.</p>
					<h4>18. YOU ARE RESPONSIBLE FOR MAINTAINING THE CONFIDENTIALITY OF YOUR LOGIN ID AND PASSWORD:</h4>
					<p align="justify">You are responsible for maintaining the security of your Login ID and Password, and may not provide these credentials to any third party. If you believe that they have been stolen or been made known to others, you must contact us immediately at privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com. We are not responsible if someone else accesses your account through Registration Information they have obtained from you or through a violation by you of this Privacy and Security Policy or the K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan Terms of Use.</p>
					<p align="justify">If you have any security related concern, please contact us at privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com. We will work closely with you to ensure a rapid and personal response to your concerns.</p>
					<h4>19. ISO 27001:</h4>
					<p align="justify">ISO/IEC 27001:2013 is the international standard for management of information security and provides a systematic approach to keep sensitive company information secure. Getting the ISO 27001:2013 certificate is a reassurance for our customers that K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan complies with the highest standards regarding information security. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan is ISO/IEC 27001:2013 certified under certificate number IND17.0322/U. We have implemented the ISO/IEC 27001: 2013 standard for all processes supporting the development and delivery of services by K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.</p>
					<h4>20. CONTACT US WITH ANY QUESTIONS OR CONCERNS (GRIEVANCE REDRESSAL):</h4>
					<p align="justify">If you have grievance or complaint, questions, comments, concerns or feedback in relation to the processing of information or regarding this Privacy and Security Policy or any other privacy or security concern, send an e-mail to privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com.</p>
					<p align="justify">The name and contact details of the Grievance Officer is Harsh Upadhyay, its registered office at 101, A & 102, 1st Floor, Morya Landmark II, Andheri West, Mumbai 400053.</p>
					<h5>FEEDBACK:</h5> 
					<p align="justify">If you have an unresolved privacy or data use concern that has not been addressed satisfactorily, you are advised to contact https://feedback-form.truste.com/watchdog/request.</p>
					<h4>21. NOTICE:</h4>
					<p align="justify">The effective date of this Policy, as stated below, indicates the last time this Policy was revised or materially changed.</p>
					<h4>22. EFFECTIVE DATE:</h4>
					<p align="justify">April 1, 2018</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/js/validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/js/validation/form-validation.js')}}"></script>
<script src="{{URL::asset('public/js/validation/app.js')}}"></script>
<script src="{{URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script src="{{URL::asset('public/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script>
	var index = 0;
	function needs(i) {
		$('#family_social_needs_'+i).val($('#needs'+i).val());
	}
	jQuery(document).ready(function () {
		App.init();
        FormValidation.init();
        $('.multiselect').multiselect();

        add_row();
        
        $('.datepicker').datepicker({
			format: 'dd-mm-yyyy',
		});
		$('.datepicker').datepicker('setStartDate', "01-01-1900");
		/*$('#gender').change(function() {
			if($('#gender').val() == 'Other') {
				$('.gender_other').show();
			} else {
				$('#gender_other').val('');
				$('.gender_other').hide();
			}
		});
		$('#gender').trigger('change');*/
		$('.table-responsive').on('show.bs.dropdown', function () {
			 $('.table-responsive').css( "overflow", "inherit" );
		});
		$('.table-responsive').on('hide.bs.dropdown', function () {
			 $('.table-responsive').css( "overflow", "auto" );
		});
		
		$('#education').change(function() {
			if($('#education').val() == 'Other') {
				$('.other_education').show();
			} else {
				$('#other_education').val('');
				$('.other_education').hide();
			}
		});
		$('#education').trigger('change');
		
		$('#employment').change(function() {
			if($('#employment').val() == 'Other') {
				$('.employment_other').show();
			} else {
				$('#employment_other').val('');
				$('.employment_other').hide();
			}
		});
		$('#employment').trigger('change');
		
		$('#type_of_work').change(function() {
			if($('#type_of_work').val() == 'Other') {
				$('.other_type_of_work').show();
			} else {
				$('#other_type_of_work').val('');
				$('.other_type_of_work').hide();
			}
		});
		$('#type_of_work').trigger('change');
		
		$('.long_term_diseases').change(function() {
			$('#long_term_diseases').val($('.long_term_diseases').val());
		});
		
		$('.user_needs').change(function() {
			$('#social_needs_for_me').val($('.user_needs').val());
		});
		
		$('#language').change(function() {
			$('#language_known').val($('#language').val());
		});
		$('#language').trigger('change');
		
		$('#property_ownership').change(function() {
			if($('#property_ownership').val() == 'Yes') {
				$('.property_type').show();
			} else {
				$('#property_type').val('');
				$('#commercial').val('');
				$('.property_type').hide();
				$('.commercial').hide();
			}
		});
		$('#property_ownership').trigger('change');
		
		$('#property_type').change(function() {
			if($('#property_type').val() == 'Commercial') {
				$('.commercial').show();
				$('#other_property').val('');
				$('.other_property').hide();
			} else if($('#property_type').val() == 'Others') {
				$('.other_property').show();
				$('#commercial').val('');
				$('.commercial').hide();
			} else {
				$('#commercial').val('');
				$('#other_property').val('');
				$('.other_property').hide();
				$('.commercial').hide();
			}
		});
		$('#property_type').trigger('change');
		
		$('#loan').change(function() {
			if($('#loan').val() == 'Yes') {
				$('.loan_type').show();
			} else {
				$('#loan_type').val('');
				$('#other_loan').val('');
				$('.loan_type').hide();
				$('.other_loan').hide();
			}
		});
		$('#loan').trigger('change');
		
		$('#loan_type').change(function() {
			if($('#loan_type').val() == 'Others') {
				$('.other_loan').show();
			} else {
				$('#other_loan').val('');
				$('.other_loan').hide();
			}
		});
		$('#loan_type').trigger('change');
		
		$('#insurance').change(function() {
			if($('#insurance').val() == 'Yes') {
				$('.insurance_type').show();
			} else {
				$('#insurance_type').val('');
				$('#other_insurance').val('');
				$('#insured_company_name').val('');
				$('.insurance_type').hide();
				$('.other_insurance').hide();
			}
		});
		$('#insurance').trigger('change');
		
		$('#insurance_type').change(function() {
			if($('#insurance_type').val() == 'Others') {
				$('.other_insurance').show();
			} else {
				$('#other_insurance').val('');
				$('.other_insurance').hide();
			}
		});
		$('#insurance_type').trigger('change');
		
		$('#vehicle').change(function() {
			if($('#vehicle').val() == 'Other') {
				$('.other_vehicle').show();
			} else {
				$('#other_vehicle').val('');
				$('.other_vehicle').hide();
			}
		});
		$('#vehicle').trigger('change');
		
		$('#house').change(function() {
			if($('#house').val() == 'Other') {
				$('.other_house').show();
			} else {
				$('#other_house').val('');
				$('.other_house').hide();
			}
		});
		$('#house').trigger('change');
		
	});
	
	function formsubmit(form){
		//$('#loading').show();
		form.submit();
	}
	
	function add_row() {
		var html = '';
		html += '<tr id="row'+index+'">';
			html += '<td>';
				html += '<button class="btn btn-default" type="button" onclick="remove_row('+index+')">-</button>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_name[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_age[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_relationship[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_employment[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_mobile[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<select class="form-control fm multiselects" multiple="multiple" data-plugin-multiselect onchange="needs('+index+')" id="needs'+index+'">';
				@foreach ($categories as $category)
					html += '<optgroup label="{{ $category->category_name }}">';
					@foreach ($category->subCategories as $need)
						html += '<option value="{{ $need->id }}">{{ $need->sub_category_name }}</option>';
					@endforeach
					html += '</optgroup>';
				@endforeach
				html += '</select>';
				html += '<input type="hidden" class="form-control" id="family_social_needs_'+index+'" name="family_social_needs[]">';
			html += '</td>';
		html += '</tr>';
		$("#tbltable").append(html);
		$('.multiselects').multiselect();
		index++;
	}
	
	function remove_row(i) {
		var r = confirm("Do you want to remove this row ?");
		if (r == true) {
			$("#row"+i).remove();
		}
	}
	
	$('#state').change(function(){
		var value = $(this).val();
		$("#city option,#tehsil option,#grampanchayat option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getdistrict')}}",
                data:{'state_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#city").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#city").append(
				      		$("<option></option>").text(item.district_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});

	$('#city').change(function(){
		var value = $(this).val();
		$("#tehsil option,#grampanchayat option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('gettehsil')}}",
                data:{'district_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#tehsil").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#tehsil").append(
				      		$("<option></option>").text(item.tehsil_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});

	$('#tehsil').change(function(){
		var value = $(this).val();
		$("#grampanchayat option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getgrampanchayat')}}",
                data:{'tehsil_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#grampanchayat").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#grampanchayat").append(
				      		$("<option></option>").text(item.panchayat_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});
</script>
@endsection


