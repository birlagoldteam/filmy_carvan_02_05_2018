@extends('layouts.app')
@section('page_title')
    Register
@endsection
@section('content')
    <section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Register</h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">Register</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <form action="{{ route('resetpassword') }}"  method="post" id="check">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="container">
            <div class="row">
                <div class="col-md-12" align="center">
                    <div class="featured-boxes">
                        <div class="row">                       
                            <div class="col-sm-2"></div> 
                            <div class="col-sm-8">
                                <div class="featured-box featured-box-primary align-left mt-xlg">
                                    <div class="box-content">
                                        <h4 class="heading-primary text-uppercase mb-md">Forgot Password</h4>
                                        
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>Password</label>
                                                        <input type="password" value="" class="form-control input-lg" name="password" id="password" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>Re-enter Password</label>
                                                       
                                                        <input type="password" value="" class="form-control input-lg" name="confirmpassword" id="confirmpassword" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary pull-right mb-xl">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2"></div> 
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form> 
   <!--  <<div class="modal fade" id="emailsend" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Otp Verification</h4>
                </div>
                <div class="modal-body">
                    Please Enter The Otp on Your Mobile No.        
                    <input type="text" value="" data-msg-required="Please enter your otp." data-msg-email="Please enter a valid Otp." maxlength="100" class="form-control" name="otpffghf" id="otpffghf">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green" onclick="checkotp()">Resend Otp</button>
                    <button type="button" class="btn green" id="registeruser">Validate</button>
                </div>
            </div>
        </div>
    </div> -->
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/js/validation/jquery.validate.js')}}"></script>
    <script src="{{URL::asset('public/js/validation/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/js/validation/app.js')}}"></script>
    <script>
        jQuery(document).ready(function(){
            App.init();
            FormValidation.init();
        });
    </script>
@endsection         
