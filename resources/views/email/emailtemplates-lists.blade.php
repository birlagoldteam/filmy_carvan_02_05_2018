@extends('layout.master')
@section('page_title')
    Email Template List
@endsection
@section('page_level_style_top')
    <header class="page-header">
        <h2>Email Template List</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Dashboard</span></li>
                <li><span>Email Template List</span></li>
            </ol>
            <a class="sidebar-right-toggle" data-open="#"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
@endsection
@section('content')
    <section role="main" class="content-body">
        <section class="panel">
        	<header class="panel-heading">
        		<div class="row">
        			<h2 class="panel-title col-md-4">Email Template List</h2>
        		</div>
        	</header>
        	<div class="panel-body">
				<div class="col-md-12" style="margin-bottom: 20px;">
					<div class="table-scrollable">
						<table class="table table-striped table-hover table-bordered" >
							<thead>
								<tr>
									<th>
										#
									</th>
									<th>
									    Title 
									</th>
									<th>
									    Subject
									</th>
									<th>
									    Status
									</th>
									<th>
									    Content
									</th>
									<th>
										Action
									</th> 
								</tr>
							</thead>
							<tbody>
								@if($emaillist->count()>0)
									<?php $arr=[];?>
									@foreach($emaillist as $key=>$emaillists)
										<tr>
											<td>
												{{$key+1}}
											</td>
											<td>
												{{$emaillists->title}}
											<td>
												{{$emaillists->subject}}
											</td>
											<td>
												{{ ucfirst($emaillists->status) }}
											</td>
											<td>
												<a data-id="{{ $emaillists->id }}" class="btn datacontent">Content</a>
											</td>
											<td>
												<!--a href="{{ route('email-template-edit',$emaillists->email_template_id)}}?page=&q={{ $search }}&status={{ $status }}">Edit |</a-->
												<a href="{{ route('email-template-status',$emaillists->id)}}">
													{{ $emaillists->status=="active" ? "Deactive" : "Active" }}
												</a>  								
											</td> 
										</tr>
										<?php $arr[$emaillists->id]=$emaillists->content;?>
									@endforeach
								@endif
							</tbody>
						</table>
						<div class ="pull-right" style="margin-bottom:20px">
	                		{{ $emaillist->links() }}
	           			</div> 
					</div>
				</div>
			</div>
		</section>
	</section>
	<div class="modal fade" id="emailcontent" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Email Content</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row form-group">
                            <div class="col-md-12">
                                <textarea class="ckeditor form-control" name="content" id="editor1" rows="6" data-error-container="#editor2_error"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/assets/ckeditor/ckeditor.js')}}"></script>
   	<script>
   		var data = {!! json_encode($arr) !!}
        jQuery(document).ready(function (){
        	$('#loading').hide(); 
        });

        $('.datacontent').click(function(){
        	$('#loading').show(); 
        	var value = $(this).data('id');
        	CKEDITOR.instances.editor1.setData(data[value]);
        	$('#emailcontent').modal('toggle');
        	$('#loading').hide();
        })
	</script>
@endsection