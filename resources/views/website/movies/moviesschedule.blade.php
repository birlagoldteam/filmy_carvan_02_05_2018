@extends('layouts.app')
@section('page_title')
    Movies Schedule
@endsection
@section('content')
   <section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Movies Schedule<!-- <span>Listings for Sale or Rent - 123 properties --></h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">Movies Schedule</li>
                    </ul>
                </div>
            </div>
            <div class="row mt-lg">
                <div class="col-md-12">

                    <form id="propertiesForm" action="#" method="POST">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-control-custom mb-md">
                                    <select class="form-control text-uppercase font-size-sm" name="propertiesPropertyType" data-msg-required="This field is required." id="propertiesPropertyType" required="">
                                        <option value="">State</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-control-custom mb-md">
                                    <select class="form-control text-uppercase font-size-sm" name="propertiesLocation" data-msg-required="This field is required." id="propertiesLocation" required="">
                                        <option value="">District</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-control-custom mb-md">
                                    <select class="form-control text-uppercase font-size-sm" name="propertiesMinBeds" data-msg-required="This field is required." id="propertiesMinBeds" required="">
                                        <option value="">Tehsil</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-control-custom mb-md">
                                    <select class="form-control text-uppercase font-size-sm" name="propertiesMinPrice" data-msg-required="This field is required." id="propertiesMinPrice" required="">
                                        <option value="">Gram Panchayat</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-control-custom mb-md">
                                    <select class="form-control text-uppercase font-size-sm" name="propertiesMaxPrice" data-msg-required="This field is required." id="propertiesMaxPrice" required="">
                                        <option value="">Village</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <input type="submit" value="Search Now" class="btn btn-secondary btn-lg btn-block text-uppercase font-size-sm">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <section class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions">
                            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                            <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                        </div>
                
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="demo-real-estate.html">State</a></li>
                        <li>District</li>
                        <li>Tehsil</li>
                        <li>Gram Panchayat</li>
                        <li class="active">Village</li>
                    </ul>
                    </header>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped" id="datatable-ajax" data-url="admin/assets/ajax/ajax-datatables-sample.json">
                            <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th width="30%">Movie</th>
                                    <th width="30%">Location (Area)</th>
                                    <th width="15%">Timimgs</th>
                                    <th width="15%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5">No Record Found!!</td>
                                    <!-- <td width="30%">Padmavati</td>
                                    <td width="30%">Near SBI Bank</td>
                                    <td width="15%">7.30 PM</td>
                                    <td width="15%">On Time</td> -->
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>  
    </div>
@endsection