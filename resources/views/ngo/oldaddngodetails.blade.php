@extends('layouts.app')
@section('page_title')
    Members
@endsection
@section('page_level_style_top')
<link rel="stylesheet" href="{{ URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('public/vendor/bootstrap-datepicker/bootstrap-datepicker3.css')}}" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('content')
   <section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Ngo Registration</h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">Registration</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
            
    <div class="container">
        <div id="membership" class="heading heading-border heading-middle-border heading-middle-border-center">
            <h2><strong>Ngo Registration</strong></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="agent-item agent-item-detail">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="ngoRegistrationForm" name="ngoRegistrationForm" action="{{ url('add-ngo') }}" method="post" enctype="multipart/form-data"  files=true>
                                {{ csrf_field() }}
                                <input type="hidden" value="Contact Form" name="subject" id="subject">
                                
                                <div class="row">
                                    <!-- <h4>1. Registration Details.</h4> -->
                                    <div class="form-group">
                                        <div class="col-md-4">
                                           <label>Unique Id Of VO/NGo *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="unique_id" id="unique_id" 
                                            required>
                                        </div>
                                        <!-- <P>@{{ngo.unique_id}}</P> -->
                                         <div class="col-md-4">
                                           <label>Name Of VO/NGo *</label>
                                            <input type="text" value="" data-msg-="Please enter your name." maxlength="100" class="form-control" name="name" id="name">
                                        </div>
                                    </div>
                                    <h4>1. Registration Details.</h4>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Registered With *</label>
                                            <input type="text" value="" data-msg-="Please enter your name." maxlength="100" class="form-control" name="registered_with" id="registered_with">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Type Of NGO *</label>
                                            <select class="form-control mb-md" 
                                            name="ngo_type">
                                                <option>---</option>
                                                <option value="NGO CSR">NGO CSR</option>
                                                <option value="CSR Group">CSR Group</option>
                                                <option value="Limited Company" >Limited Company</option>
                                                <option value="Private Company"> Private Company</option>
                                                <option value="Partner">Partner</option>
                                                <option value="Individual">Individual</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Registration NO*</label>
                                            <input type="text" value="" data-msg-="Please enter your name." maxlength="100" class="form-control" name="registration_no" id="registration_no">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Copy Of Registration Certificate *</label>
                                            <select class="form-control mb-md" 
                                            name="registration_certificate" id="registration_certificate" >
                                                <option>---</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                            <input type="file" value="" data-msg-="Please enter your name." maxlength="100" class="form-control" name="registration_name" id="registration_name" style="display:none;">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Copy Of Pan Card *</label>
                                            <select class="form-control mb-md" name="pan" id="pan">
                                                <option>---</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                            <input type="file" value="" data-msg-="Please enter your name." maxlength="100" class="form-control" name="pan_name" id="pan_name"  style="display:none;">
                                        </div>

                                        <div class="col-md-4">
                                            <label>Act Name. *</label>
                                            <input type="text" value="" data-msg-="Please enter your name." maxlength="100" class="form-control" name="act_name" ng_model="ngo.act_name" 
                                            id="act_name" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>State Of Registration  *</label>
                                            <select class="form-control mb-md" name="ngo_register_state" id="ngo_register_state">
												<option value="">---</option>
												@foreach($state as $value)
													<option value="{{$value->id}}">{{$value->state_name}}</option>
												@endforeach
											</select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>City Of Registration *</label>
                                             <select class="form-control mb-md" name="ngo_register_city" id="ngo_register_city">
												<option value="">---</option>
											</select>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Date Of Registration *</label>
                                            <input value="" style="border-radius: 0px !important;" type="text" class="form-control datepicker"  name="reg_date" id="reg_date" readonly>
                                        </div>
                                      
                                    </div>
                                  
                                   <h4>2. Members.</h4>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped table-condensed mb-none">
                                                    <thead>
                                                        <tr>
															<th width="5%">&nbsp;</th>
                                                            <th>Name</th>
                                                            <th>Designation</th>
                                                            <th>Pan</th>
                                                            <th>Adhaar</th>
                                                            <th>Email</th>
                                                            <th>Mobile Number</th>
                                                            <th>Profile Upload</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbltable">
													
													</tbody>
                                                    <tfoot>
														<tr>
															<td colspan="8"><button type="button" class="btn btn-default" onclick="add_row()">+</button></td>
														</tr>
													</tfoot>
                                                </table>
                                            </div>
                                        </div>  
                                    </div>

                                    <h4>3. Sector/Key Issues.</h4>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Key Issues *</label>
                                            <input type="text" value="" data-msg-="Please enter your name." maxlength="100" class="form-control" name="key_issues" id="key_issues">
                                        </div>
                                         <div class="col-md-4">
											<label>Operation Area-Districts *</label><br />
											<select class="form-control multiselect operational_area_cities" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": true }'>
											@foreach ($operational_states as $states)
											  <optgroup label="{{ $states->state_name }}">
											  @foreach ($states->districts as $districts)
												 <option value="{{ $districts->id }}">{{ $districts->district_name }}</option>
											  @endforeach
											  </optgroup>
											@endforeach
											</select>
											<input type="text" value="" class="form-control" id="operational_area_cities" name="operational_area_cities">
										</div>
                                        <!--<div class="col-md-4">
                                            <label>Operation Area-States *</label>
                                            <select class="form-control mb-md"
                                                name="operation_area_state" 
                                                id="operation_area_state">
												<option value="">---</option>
												@foreach($state as $value)
													<option value="{{$value->id}}">{{$value->state_name}}</option>
												@endforeach
											</select>
                                        </div>
                                        <div class="col-md-4">
											<label>Operation Area-Districts *</label>
                                             <select class="form-control mb-md" 
                                             name="operation_area_city" id="operation_area_city">
												<option value="">---</option>
											</select>
                                        </div>-->
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>No Of Volunteer *</label>
                                            <input type="text" value="" data-msg-="Please enter your name." maxlength="100" class="form-control" name="volunteer_no" id="volunteer_no">
                                        </div>
                                    </div>

                                     <h4>4. Fund Detail.</h4>
                                    <div class="form-group">
                                         <div class="col-md-4">
                                            <label>Funds Status *</label>
                                            <select class="form-control mb-md" 
                                                name=" fund_status">
                                                <option>---</option>
                                                <option>Available</option>
                                                <option>Not Adequate</option>
                                                <option>Not Available</option>
                                            </select>
                                        </div>
                                    </div>

                                    <h4>5. FCRA Details.</h4>
                                    <div class="form-group">
                                         <div class="col-md-4">
                                            <label>FCRA Available *</label>
                                            <select class="form-control mb-md" name="fcra" id="fcra">
                                                <option>---</option>
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>
                                            </select>
                                        </div>
                                         <div class="col-md-4 fcra" style="display:none;">
                                            <label>FCRA Registraton No *</label>
                                            <input type="text" value="" data-msg-="Please enter your email address." data-msg-email="Please enter a registraton no." maxlength="100" class="form-control" name="fcrano" id="fcrano">
                                        </div>
                                      
                                    </div>

                                     <h4>6. Details Of Achievements.</h4>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Skill Development *</label>
                                            <select class="form-control mb-md" name="skill">
                                                <option>---</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                    </div>


                                    <h4>5. Contact Details.</h4>
                                    <div class="form-group">
                                         <div class="col-md-4">
                                            <label>Address *</label>
                                            <input type="text" value="" data-msg-="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="address" id="address">
                                        </div>
                                         <div class="col-md-4">
                                            <label>State *</label>
                                            <select class="form-control mb-md" name="state" id="state">
												<option value="">---</option>
												@foreach($state as $value)
													<option value="{{$value->id}}">{{$value->state_name}}</option>
												@endforeach
											</select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>City *</label>
                                             <select class="form-control mb-md" name="city" id="city">
												<option value="">---</option>
											</select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                         <div class="col-md-4">
                                            <label>Telephone No *</label>
                                             <input type="text" value="" data-msg-="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="telephone" id="telephone" name="telephone">
                                        </div>
                                         <div class="col-md-4">
                                            <label>Mobile No *</label>
                                            <input type="text" value="" data-msg-="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="mobile" id="mobile">
                                        </div>
                                         <div class="col-md-4">
                                            <label>URL Website *</label>
                                            <input type="text" value="" data-msg-="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="url" id="url">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                         <div class="col-md-4">
                                            <label>E-mail *</label>
                                            <input type="email" value="" data-msg-="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email">
                                    </div>

                                  <!--  <p> @{{ngo.email}} </p> -->
                                
                                </div>
                               </br>
                            
                               <!--  <div class="row"> -->
                                    <div class="col-md-12">
                                        <input type="submit" value="Submit" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
                                    <!-- </div> -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
@endsection
@section('page_level_script_bottom')
<!--
<script src="{{URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
-->
<script src="{{URL::asset('public/js/validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/js/validation/form-validation.js')}}"></script>
<script src="{{URL::asset('public/js/validation/app.js')}}"></script>
<script src="{{URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script src="{{URL::asset('public/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script>
	var index = 0;
	function needs(i) {
		$('#family_social_needs_'+i).val($('#needs'+i).val());
	}
	jQuery(document).ready(function () {
		App.init();
        FormValidation.init();
        $('.multiselect').multiselect();
         // $('.multiselect').multiselect();
         // $('.multiselects').multiselect();
        add_row();
        
        $('.datepicker').datepicker({
			format: 'dd-mm-yyyy',
		});
		$('.datepicker').datepicker('setStartDate', "01-01-1900");
        
        $('#registration_certificate').change(function() {
            if($(this).val() == 'Yes') {
                $('#registration_name').show();
            } else {
                $('#registration_name').hide();
            }
        });
        $('#pan').change(function() {
            if($(this).val() == 'Yes') {
                $('#pan_name').show();
            } else {
                $('#pan_name').hide();
            }
        });

        $('.datepicker').datepicker({
			format: 'dd-mm-yyyy'
		});
		
		$('.operational_area_cities').change(function() {
			$('#operational_area_cities').val($('.operational_area_cities').val());
		});
		$('.operational_area_cities').trigger('change');
		
		$('#ngo_register_state').trigger('change');
        $('#operation_area_state').trigger('change');
        $('#state').trigger('change');
        
        $('#fcra').change(function() {
            if($(this).val() == 'Yes') {
                $('.fcra').show();
            } else {
				$('#fcrano').val('');
                $('.fcra').hide();
            }
        });
        $('#registration_certificate').trigger('change');
	});
		
		
	function formsubmit(form){
		//$('#loading').show();
		form.submit();
	}
	
	function add_row() {
		var html = '';
		html += '<tr id="row'+index+'">';
			html += '<td>';
				html += '<button class="btn btn-default" type="button" onclick="remove_row('+index+')">-</button>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_name[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_designation[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_pan[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_aadhar[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_email[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="member_mobile[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="file" value="" class="form-control" name="member_profile_upload[]" required>';
			html += '</td>';
		html += '</tr>';
		$("#tbltable").append(html);
		$('.multiselects').multiselect();
		index++;
	}
	
	function remove_row(i) {
		var r = confirm("Do you want to remove this row ?");
		if (r == true) {
			$("#row"+i).remove();
		}
	}
	
	$('#ngo_register_state').change(function(){
		var value = $(this).val();
		$("#ngo_register_city option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getdistrict')}}",
                data:{'state_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#ngo_register_city").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#ngo_register_city").append(
				      		$("<option></option>").text(item.district_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});
	
	$('#operation_area_state').change(function(){
		var value = $(this).val();
		$("#operation_area_city option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getdistrict')}}",
                data:{'state_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#operation_area_city").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#operation_area_city").append(
				      		$("<option></option>").text(item.district_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});
	
	$('#state').change(function(){
		var value = $(this).val();
		$("#city option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getdistrict')}}",
                data:{'state_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#city").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#city").append(
				      		$("<option></option>").text(item.district_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});

	
</script>
@endsection
