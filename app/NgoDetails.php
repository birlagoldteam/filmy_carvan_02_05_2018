<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NgoDetails extends Model
{
	protected $table = 'ngo_details';
	
	protected $primaryKey = 'id';
	
	public function ngoMembers() {
		return $this->hasMany('App\NgoMembers', 'user_id', 'user_id');
	}
}
