<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tehsil extends Model
{
    protected $table = 'tehsil';
	
	protected $fillable = ['state_id','district_id','tehsil_name'];
}
