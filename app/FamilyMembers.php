<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FamilyMembers extends Model
{
	protected $table = 'family_members';
    
    protected $primaryKey = 'id';
    
    public function memberships() {
        return $this->belongsTo('App\Memberships','user_id');
    }

}
