<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('includes.header', function ($view) {
            if(!Session::has('state')){
                $state = app('App\Http\Controllers\Controller')->getStates();
            }else{
                $state = Session::get('state');
            }
            $view->with('statedata',$state);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
