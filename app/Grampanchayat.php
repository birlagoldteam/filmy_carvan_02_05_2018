<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grampanchayat extends Model
{
    protected $table = 'grampanchayat';
	
	protected $fillable = ['state_id','district_id','tahsil_id','panchayat_name'];
}
