<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Http\Controllers\Controller;
use App\Register;
use App\Otp;
use DateTime;
use Excel;
use DB;
use Session;
use App\User;
use App\Categories;
use App\FamilyMembers;
use App\Memberships;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;


class TestController extends Controller {

    public function generateotp(Request $request){
        //$validator = Validator::make($request->all(),['mobile'=>'required']);         
        $arr['success'] = true;
        $arr['error']='';
        $arr['data']['user'] = '';
        $mobile = $request['mobile'];
        $data = User::where('mobile',$mobile)
                    ->first();
        if(!empty($data)){
            $arr['data']['user'] = $data->id;
            $arr['data']['name'] = $data->name;
            $arr['data']['email'] = $data->email;
            $arr['data']['mobile'] = $data->mobile;         
            $arr['error']='';
            $arr['data']['otp'] = '';
            return $arr;
        }
        $num_str = sprintf("%06d",mt_rand(1,999999));    
        $arr['data']['otp'] = $num_str;
        $opt = Otp::where(['number'=>$request['mobile']]);
        $check = clone ($opt);
        if(!empty($opt->first())){
            $check->update(['otp'=>$num_str]);

        }else{
            $otpObj = new Otp();
            $otpObj->number = $request['mobile'];
            $otpObj->otp = $num_str;
            $otpObj->save();
        }
        $message="$num_str is the otp for filmy caravan";
        $this->sendsms($mobile,$message);
         return ['success'=>true,'error'=>'','data'=>['user'=>'','mobile'=>'','email'=>'','name'=>'' ,'otp'=>$num_str]];
    }




    public function register(Request $data){
        $userdata = User::where('mobile',$data->mobile)
                        ->first();
        if(!empty($userdata)){
           return ['success'=>false,'error'=>'Mobile No Already Exists!!!','data'=>['user'=>$userdata->id]];
        }
        //try{
            $user = User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'mobile'=>$data['mobile'],
                    'password' => bcrypt("123456"),
                    'aadhar_number'=>$data['aadhar_number']
            ]);
            return $arr=['success'=>true,'error'=>'','data'=>['user'=>$user->id]];
       // }catch(\Illuminate\Database\QueryException $ex){


            //return ['success'=>false,'error'=>'Something went Wrong!!!','data'=>['user'=>'']];
        //}
       // return ['success'=>true,'error'=>'','data'=>''];
       
    }

    public function membership_register(Request $request){
        $data = Memberships::where('user_id',$request->user_id)
                           ->first();
        if(empty($data)){
            $membership = new Memberships();    
        }else{
            $membership = Memberships::findOrFail($data->id);    

        }
        $membership->user_id = $request['user_id'];
        $membership->name = $request['name'];
        $membership->email = $request['email'];
        $membership->mobile = $request['mobile'];
        $membership->gender = $request['gender'];
        $membership->gender_other = $request['gender_other'];
        //$membership->date_of_birth = !empty($request['date_of_birth']) ? date('Y-m-d',strtotime($request['date_of_birth'])) : null;
        $membership->aadhar_number = $request['aadhar_number'];
        $membership->pan = $request['pan'];
        $membership->marital_status = $request['marital_status'];
        $membership->children = $request['children'];
        $membership->blood_group = 1;//$request['blood_group'];
        //$membership->country = $request['country'];
        $membership->state = $request['state'];
        $membership->city = $request['city'];
        $membership->tehsil = $request['tehsil'];
        $membership->grampanchayat = $request['grampanchayat'];
        $membership->village = $request['village'];
        $membership->street_area = $request['street_area'];
        $membership->house_name_no = $request['house_name_no'];
        $membership->pincode = $request['pincode'];
        $membership->education = $request['education'];
        $membership->language_known = $request['language_known'];
        $membership->employment = $request['employment'];
        $membership->employment_other = $request['employment_other'];
        $membership->handicapped = $request['handicapped'];
        $membership->long_term_diseases = $request['long_term_diseases'];
        $membership->social_needs_for_me = $request['social_needs_for_me'];
        $membership->family_annual_income = $request['family_annual_income'];
        $membership->monthly_expenses = $request['monthly_expenses'];
        //$membership->property_type = $request['property_type'];
        $membership->commercial = $request['commercial'];
        $membership->other_property = $request['other_property'];
        //$membership->property_ownership = $request['property_ownership'];
        $membership->property_type = $request['property_type'];
        $membership->commercial = $request['commercial'];
        $membership->other_property = $request['other_property'];
        $membership->vehicle = $request['vehicle'];
        $membership->other_vehicle = $request['other_vehicle'];
        //$membership->loan = $request['loan'];
        $membership->loan_type = $request['loan_type'];
        $membership->other_loan = $request['other_loan'];
        //$membership->insurance = $request['insurance'];
        $membership->insurance_type = $request['insurance_type'];
        $membership->other_insurance = $request['other_insurance'];
        //echo json_encode($membership);
        //die;
        $membership->save();
        $arr = array();
        $arrkey=['family_name1','family_name2','family_name3','family_name4','family_name5'];
        FamilyMembers::where('user_id',$request->user_id)->delete();
        foreach($arrkey as $key=>$value){
            if(array_key_exists($value,$request->all())){
                $arr[$key]['user_id'] = $request->user_id;
                $arr[$key]['name'] = $request->{'family_name'.($key+1)};
                $arr[$key]['age'] = $request->{'family_age'.($key+1)};
                $arr[$key]['relationship'] = $request->{'family_relationship'.($key+1)};
                $arr[$key]['employment'] = $request->{'family_employment'.($key+1)};
                $arr[$key]['mobile'] = $request->{'mobile'.($key+1)};
                $arr[$key]['social_needs'] = $request->{'social_needs'.($key+1)};
            }
        }
        if(count($arr)>0){
            FamilyMembers::insert($arr);
        }
        return  ['success'=>true,'error'=>'','data'=>['user'=>$request->user_id,'membership'=>$membership->id]];

       
    }

    public function social_issue(){
        return Categories::with('subCategories')->get();
    }

    public function state(){
      //  return $this->getStates();
        return $state =$this->getStates();       
        return ['success'=>true,'error'=>'','data'=>$this->getStates()];  
    }

    public function district(Request $request){

        $id = $request->state_id;
        return $this->getDistrict($id);
    }

    public function tehsil(Request $request){
        $id = $request->district_id;
        return $this->getTehsil($id);
    }

    public function grampanchayat(Request $request){
        $id = $request->tehsil_id;
        return $this->getgrampanchayat($id);
    }

    public function terms_conditions() {
        return '<p align="justify">Welcome to FILMY CARAVAN Membership Registration (Application name & Website/Mobile App/E Registration Form Address) owned and operated by K Sera Sera Box Office P Limited (KSS Box Office/Filmy Caravan), a company incorporated under the provisions of the Companies Act, 1956 and having its registered office at 101, A & 102, 1st Floor, Morya Landmark II, Andheri West, Mumbai 400053.</p>
        <p align="justify">This document/agreement/understanding is an electronic record in terms of Information Technology Act, 2000 and is generated by a computer system and does not require any physical or digital signatures. This document is published in accordance with the provisions of Rule 3 of the Information Technology (Intermediaries Guidelines) Rules, 2011.</p>
        <p align="justify">Before you may use the Website/Mobile App/E Registration Form, you must read all of the terms and conditions (“Terms”) herein and the Privacy Policy provided on the Website/Mobile App/E Registration Form. By using KSS Box Office/Filmy Caravan products, software, services and Website/Mobile App/E Registration Form (“Services”), you understand and agree that KSS Box Office/Filmy Caravan will treat your use of the Services as acceptance of these terms from such point of usage. You may not use the Services if you do not accept the Terms. If you do not agree to be bound by these Terms and the Privacy Policy, you may not use the Website/Mobile App/E Registration Form in any way. It is strongly recommended for you to return to this page periodically to review the most current version of the Terms. KSS Box Office/Filmy Caravan reserves the right at any time, at its sole discretion, to change or otherwise modify the Terms without prior notice, and your continued access or use of this Website/Mobile App/E Registration Form signifies your acceptance of the updated or modified Terms. If you object to these Terms or any subsequent modifications to these Terms or become dissatisfied with the Website/Mobile App/E Registration Form in any way, your only recourse is to immediately terminate use of the Website/Mobile App/E Registration Form.</p>

        <h4>1.  Proprietary rights</h4>
        <p align="justify">You acknowledge and agree that KSS Box Office/Filmy Caravan owns all rights, title and interest in and to the Services, including any intellectual property rights which subsist in the Services (whether registered or not). You further acknowledge that the Services may contain information which is designated confidential by KSS Box Office/Filmy Caravan and that you shall not disclose such information without KSS Box Office/Filmy Caravans prior written consent.</p>
        <p align="justify">KSS Box Office/Filmy Caravan grants you a limited license to access and make personal use of the Website/Mobile App/E Registration Form and the Services. This license does not confer any right to download, copy, create a derivative work from, modify, reverse engineer, reverse assemble or otherwise attempt to discover any source code, sell, assign, sub-license, grant a security interest in or otherwise transfer any right in the Services. You do not have the right to use any of KSS Box Office/Filmy Caravans trade names, trademarks, service marks, logos, domain names, and other distinctive brand features. You do not have the right to remove, obscure, or alter any proprietary rights notices (including trademark and copyright notices), which may be affixed to or contained within the Services. You will not copy or transmit any of the Services.</p>
        <h4>2. Usage of the Website/Mobile App/E Registration Form</h4>
        <p align="justify">The Website/Mobile App/E Registration Form is intended for personal / Family level Need Analysis and commercial, non-commercial use intended for Social Need Analysis as well Social Development Projects. Only register to become a member of the Website/Mobile App/E Registration Form if you are above the age of 18 and can enter into binding contracts. You are responsible for maintaining the secrecy of your passwords, login and account information. You will be responsible for all use of the Website/Mobile App/E Registration Form by you and anyone using your password and login information (with or without your permission).</p>
        <p align="justify">You also agree to provide true, accurate, current and complete information about yourself as prompted by the Website/Mobile App/E Registration Form. If you provide any information that is untrue, inaccurate, not updated or incomplete (or becomes untrue, inaccurate, not updated or incomplete), or KSS Box Office/Filmy Caravan has reasonable grounds to suspect that such information is untrue, inaccurate, not updated or incomplete, KSS Box Office/Filmy Caravan has the right to suspend or terminate your account and refuse any and all current or future use of the Website/Mobile App/E Registration Form (or any portion thereof) or Services in connection thereto.</p>
        <p align="justify">By making use of the Website/Mobile App/E Registration Form, and furnishing your personal/contact details, you hereby agree that you are interested in availing and purchasing the Services that you have selected. You hereby agree that KSS Box Office/Filmy Caravan may contact you either electronically or through phone, to understand your interest in the selected products and Services and to fulfil your demand or complete your application. You specifically understand and agree that by using the Website/Mobile App/E Registration Form you authorize KSS Box Office/Filmy Caravan and its affiliates and partners to contact you for any follow up calls in relation to the Services provided through the Website/Mobile App/E Registration Form. You expressly waive the Do Not Call registrations on your phone/mobile numbers for contacting you for this purpose. You also agree that KSS Box Office/Filmy Caravan reserves the right to make your details available to partners and you may be contacted by the partners for information and for sales through email, telephone and/or sms. You agree to receive promotional materials and/or special offers from KSS Box Office/Filmy Caravan through emails or sms.</p>
        <p align="justify">The usage of the Website/Mobile App/E Registration Form may also require you to provide consent for keying in your Sensitive Information (“SI”) (including but not limited to user ids and passwords), as may be necessary to process your application through this Website/Mobile App/E Registration Form. SI keyed in shall be required for enabling hassle free, faster and paperless (to the extent possible) processing of applications for financial products so opted / availed by you. KSS Box Office/Filmy Caravan shall adhere to best industry practices including information security, data protection and privacy law while processing such applications and the SI interface where key in of SI is required shall not be stored. However, KSS Box Office/Filmy Caravan shall not be liable to you against any liability or claims which may arise out of such transactions.</p>
        <p align="justify">You may only use the Website/Mobile App/E Registration Form to search for and to apply for loans, mutual funds, credit cards or other financial products as may be displayed on the Website/Mobile App/E Registration Form from time to time and you shall not use the Website/Mobile App/E Registration Form to make any fraudulent applications. You agree not to use the Website/Mobile App/E Registration Form for any purpose that is unlawful, illegal or forbidden by these Terms, or any local laws that might apply to you. Since the Website/Mobile App/E Registration Form is in operation in India, while using the Website/Mobile App/E Registration Form, you shall agree to comply with laws that apply to India and your own country (in case of you being a foreign national). We may, at our sole discretion, at any time and without advance notice or liability, suspend, terminate or restrict your access to all or any component of the Website/Mobile App/E Registration Form.</p>
        <p align="justify">You are prohibited from posting or transmitting to or through this Website/Mobile App/E Registration Form: (i) any unlawful, threatening, libelous, defamatory, obscene, pornographic, or other material or content that would violate rights of publicity and/or privacy or that would violate any law; (ii) any commercial material or content (including, but not limited to, solicitation of funds, advertising, or marketing of any good or services); and (iii) any material or content that infringes, misappropriates or violates any copyright, trademark, patent right or other proprietary right of any third party. You shall be solely liable for any damages resulting from any violation of the foregoing restrictions, or any other harm resulting from your posting of content to this Website/Mobile App/E Registration Form.</p>
        <h4>3. Privacy policy</h4>
        <p align="justify">By using the Website/Mobile App/E Registration Form, you hereby consent to the use of your information as we have outlined in our Privacy Policy.</p>
        <h4>4. Our partners</h4>
        <p align="justify">Display of loan, mutual funds, credit card or other financial products, offered by third parties, on the Website/Mobile App/E Registration Form does not in any way imply, suggest, or constitute any sponsorship, recommendation, opinion, advice or approval of KSS Box Office/Filmy Caravan against such third parties or their products. You agree that KSS Box Office/Filmy Caravan is in no way responsible for the accuracy, timeliness or completeness of information it may obtain from these third parties. Your interaction with any third party accessed through the Website/Mobile App/E Registration Form is at your own risk, and KSS Box Office/Filmy Caravan will have no liability with respect to the acts, omissions, errors, representations, warranties, breaches or negligence of any such third parties or for any personal injuries, death, property damage, or other damages or expenses resulting from your interactions with the third parties.</p>
        <p align="justify">You agree and acknowledge that the credit shall be at the sole discretion of KSS Box Office/Filmy Caravan’s financial partners (lenders, credit card companies, mutual fund companies) while making any application through the Website/Mobile App/E Registration Form for a financial product offered by such financial partners; KSS Box Office/Filmy Caravan shall not be held liable for any delay, rejection or approval of any application made through its Website/Mobile App/E Registration Form.</p>
        <h4>5.  Disclaimer of warranty</h4>
        <p align="justify">The Website/Mobile App/E Registration Form and all content and Services provided on the Website/Mobile App/E Registration Form are provided on an "as is" and "as available" basis. KSS Box Office/Filmy Caravan expressly disclaims all warranties of any kind, whether express or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose, title, non-infringement, and security and accuracy, as well as all warranties arising by usage of trade, course of dealing, or course of performance. KSS Box Office/Filmy Caravan makes no warranty, and expressly disclaims any obligation, that: (a) the content will be up-to-date, complete, comprehensive, accurate or applicable to your circumstances; (b) The Website/Mobile App/E Registration Form will meet your requirements or will be available on an uninterrupted, timely, secure, or error-free basis; (c) the results that may be obtained from the use of the Website/Mobile App/E Registration Form or any Services offered through the Website/Mobile App/E Registration Form will be accurate or reliable; or (d) the quality of any products, services, information, or other material obtained by you through the Website/Mobile App/E Registration Form will meet your expectations.</p>
        <h4>6. Limitation of liability</h4>
        <p align="justify">KSS Box Office/Filmy Caravan (including its officers, directors, employees, representatives, affiliates, and providers) will not be responsible or liable for (a) any injury, death, loss, claim, act of god, accident, delay, or any direct, special, exemplary, punitive, indirect, incidental or consequential damages of any kind (including without limitation lost profits or lost savings), whether based in contract, tort, strict liability or otherwise, that arise out of or is in any way connected with (i) any failure or delay (including without limitation the use of or inability to use any component of the Website/Mobile App/E Registration Form), or (ii) any use of the Website/Mobile App/E Registration Form or content, or (iii) the performance or non-performance by us or any provider, even if we have been advised of the possibility of damages to such parties or any other party, or (b) any damages to or viruses that may infect your computer equipment or other property as the result of your access to the Website/Mobile App/E Registration Form or your downloading of any content from the Website/Mobile App/E Registration Form.</p>
        <p align="justify">The Website/Mobile App/E Registration Form may provide links to other third party Website/Mobile App/E Registration Forms. However, since KSS Box Office/Filmy Caravan has no control over such third party Website/Mobile App/E Registration Forms, you acknowledge and agree that KSS Box Office/Filmy Caravan is not responsible for the availability of such third party Website/Mobile App/E Registration Forms, and does not endorse and is not responsible or liable for any content, advertising, products or other materials on or available from such third party Website/Mobile App/E Registration Forms. You further acknowledge and agree that KSS Box Office/Filmy Caravan shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party Website/Mobile App/E Registration Forms.</p>
        <h4>7. Indemnity</h4>
        <p align="justify">You agree to indemnify and hold KSS Box Office/Filmy Caravan (and its officers, directors, agents and employees) harmless from any and against any claims, causes of action, demands, recoveries, losses, damages, fines, penalties or other costs or expenses of any kind or nature, including reasonable attorneys fees, or arising out of or related to your breach of this Terms, your violation of any law or the rights of a third party, or your use of the Website/Mobile App/E Registration Form.</p>
        <h4>8. Additional terms</h4>
        <p align="justify">You may not assign or otherwise transfer your rights or obligations under these Terms. KSS Box Office/Filmy Caravan may assign its rights and duties under these Terms without any such assignment being considered a change to the Terms and without any notice to you. If we fail to act on your breach or anyone else breach on any occasion, we are not waiving our right to act with respect to future or similar breaches. Other terms and conditions may apply to loans, mutual funds, credit cards or other financial products that you may apply on the Website/Mobile App/E Registration Form. You will observe these other terms and conditions. The laws of the India, without regard to its conflict of laws rules, will govern these Terms, as well as your and our observance of the same. If you take any legal action relating to your use of the Website/Mobile App/E Registration Form or these Terms, you agree to file such action only in the courts located in Chennai, India. In any such action that we may initiate, the prevailing party will be entitled to recover all legal expenses incurred in connection with the legal action, including but not limited to costs, both taxable and non-taxable, and reasonable attorney fees. You acknowledge that you have read and have understood these Terms, and that these Terms have the same force and effect as a signed agreement.</p>                     
        <h3>PRIVACY AND SECURITY POLICY</h3>

        <h4>1. INTRODUCTION:</h4>
        <p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan(hereinafter referred to as “K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan/ we/ our”) acknowledges the expectations of its customers (hereinafter referred to as “customer/ you/ your”) regarding privacy, confidentiality and security of Personal Information that resides with the K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan. Keeping Personal Information of customers secure and preventing any misuse thereof is a top priority of K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.</p>
        <p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan is strongly committed to protect the privacy of its customers and has taken all necessary and reasonable measures in line with the best industry practice to protect the confidentiality of the customer information and its transmission through Website/Mobile App/E Registration Form and it shall not be held liable for disclosure of the confidential information when in accordance with this privacy Commitment or in terms of the ‘Terms of Use’ of Website/Mobile App/E Registration Form or agreement, if any, with the customers.</p>
        <p align="justify">This Privacy & Security Policy (hereinafter referred to as “Policy”) explains how we protect Personal Information provided on our Website/Mobile App/E Registration Form www.K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com (hereinafter referred to as “Website/Mobile App/E Registration Form”) and how we use that information in connection with our service offered through the Website/Mobile App/E Registration Form (hereinafter called as "Service").</p>
        <h4>2. PERSONAL INFORMATION:</h4>
        <p align="justify">Personal Information (which may be a Sensitive Information “SI”) means any information /documents/details that relates to a natural person, which either directly or indirectly, in combination with other information available or likely to be available with the K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan, can identify such person.</p>
        <h4>3. APPLICABILITY:</h4>
        <p align="justify">This Policy is applicable to the Personal Information (including but not limited to details pertaining to your name, parentage, marital status, nationality, state of residence, city of residence, email address, date of birth, gender, contact number/mobile number, user ids, passwords, recent photograph, signature image, e-KYC through UIDAI, Aadhaar based e-Signature and other Know Your Customer (KYC) documents like address proof, identity proof, income proof, PAN or such other officially valid documents/details (OVDs) accepted for concluding a financial transaction) which are shared/ uploaded by you, as and when you avail the products and services vide the online application forms and questionnaires through usage of Website/Mobile App/E Registration Form and which K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan may become privy to. By visiting and/or using our Website/Mobile App/E Registration Form, you agree to this Policy. Further, this policy applies to all current and former visitors to the Website/Mobile App/E Registration Form and to our online customers. It is strongly recommended for you to return to this page periodically to review the most current version of the Policy. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan reserves the right at any time, at its sole discretion, to change or otherwise modify the Policy without prior notice, and your continued access or use of this Website/Mobile App/E Registration Form signifies your acceptance of the updated or modified Policy.</p> 
        <p align="justify">However, if we make any material change to the Policy, we will notify you by email (sent to the e-mail address specified in your account) or by means of a notice on this Website/Mobile App/E Registration Form prior to the change becoming effective.</p>
        <h4>4. EXPRESS CONSENT:</h4>
        <ul>
            <li>You while providing the details/documents over the Website/Mobile App/E Registration Form, including but not limited to Personal Information as mentioned hereinabove, expressly consent to K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan (its affiliates and business partners) to contact you, to make follow up calls in relation to the services provided through the Website/Mobile App/E Registration Form, for imparting product knowledge, offering promotional offers running on Website/Mobile App/E Registration Form & various other offers offered by its partners or Group companies on the Sites.</li>
            <li>The usage of the Website/Mobile App/E Registration Form may also require you to provide consent for keying in/ uploading your Personal Information and Sensitive Information (including but not limited to user ids and passwords), as may be necessary to process your application through the Website/Mobile App/E Registration Form. Sensitive information keyed in/ uploaded shall be required for enabling hassle free, faster and paperless (to the extent possible) processing of applications for financial products so opted / availed by you.</li>
            <li>You hereby authorize and expressly consent us to share your demographic and Personal Information with third parties including but not limited to Credit Information Companies to do an aggregate check of your credit profile and for K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan to send you targeted communications and offers by agreeing to the Terms of Use while accessing the Website/Mobile App/E Registration Form. You hereby also consent K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan to procure credit information and indicative scores from the credit information companies on your behalf.</li>
            <li>If you are no longer interested in sharing your Personal and Sensitive Information, please e-mail your request at: privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com. Please note that it may take about 72 business hours to process your request. In furtherance to your usage of the Website/Mobile App/E Registration Form, you expressly waive the Do Not Call (DNC) / Do Not Disturb (DND) registrations on your phone/mobile numbers for contacting you for this purpose. Hence, there is no DNC / DND check required for the number you have left on our Website/Mobile App/E Registration Form. Such modes of contacting you may include sending SMSs, email alerts and / or telephonic calls.</li>
            <li>K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan reserves the right (and you expressly authorize K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan) to share or disclose your Personal Information/ Sensitive Information when K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan determines, in its sole discretion, that the disclosure of such information is necessary or appropriate under the law for the time being in force.</li>
        </ul>
            <h4>5. OPT-OUT:</h4>
            <p align="justify">In case you do not want to be disturbed over telephonic calls, kindly fill up the details requested under this section below, includingthe details of the telephone number(s) on which you do not wish to be contacted and submit the same at privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com from your email address registered at K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan. The details that you provide through the opt-out email will remain confidential and once you have submitted the same to us, your telephone number(s) will be removed from all our telemarketing calling lists within 15 working days. We will make every effort to ensure that you do not get any further telemarketing calls on such telephone number(s). The details to be submitted for opting-out shall be as below:
            <ul>
                <li>Title*</li>   
                <li>First Name*</li>  
                <li>Last Name*</li>   
                <li>Country*</li>     
                <li>State*</li>   
                <li>City*</li>    
                <li>Email</li>    
                <li>Mobile Number #</li>  
                <li>Landline Phone Number #</li>
            </ul>
            </p>  
            <p align="justify">* Mandatory fields<br/>
            # Please enter your 10-digit mobile number. For International Numbers, please do not enter your country code</p>
            <h4>6.  PURPOSE AND USAGE:</h4>
            <p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan will not sell or rent your Personal Information to anyone, for any reason, at any time. However, we will be sharing your Personal Information/ Sensitive Information with our affiliates and business partners including credit information companies, where we feel that you will be assisted better for the purpose of underwriting and approval of your credit card, loan and approving investment in Mutual Fund or any other financial product transaction/related transaction or for doing an aggregate check of your credit profile on your behalf and ending you targeted communications and offers. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan uses the information collected and appropriately notifies you to manage its business and offer an enhanced, personalized online experience to you on its Website/Mobile App/E Registration Form. By registering on the Website/Mobile App/E Registration Form, you authorize K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan to send texts and email alerts to you and any other service requirements, including promotional mails and SMSs Further, it enables K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan to:
            <ul>
                <li>analyse usage of the Website/Mobile App/E Registration Form and to improve the Service;</li>
                <li>send you any administrative notices, offer alerts and communications relevant to your use of the Service;</li>
                <li>enable you to apply for certain products and services for which you have chosen to apply;</li>
                <li>Carry market research, project planning, troubleshooting issues, detecting and protecting against error, fraud or other criminal activity;</li>
                <li>bind third-party contractors that provide services to K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan and are bound by these same privacy restrictions;</li>
                <li>Comply with all applicable laws and regulations;</li>
                <li>enforce K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan Terms of Use.</li>
            </ul>
            </p>
            <p align="justify">In the event that you access the Service as brought to you by one of our partners either through the Website/Mobile App/E Registration Form (www.K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com) or on being redirected from a co-branded URL otherwise referred to as a white-label site, your name, e-mail address, mobile number, date of birth, employment type, residency status, income details/proofs, Form 26 AS, PAN, details of loan / credit card/ Mutual Fund applied for and loan, credit card and Mutual Fund status or any other financial product status as may be provided to that partner when your application is submitted and whenever the status of application is updated. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan has a business relationship with these partners and you may not opt-out of sharing your information with these partners if you have applied via a co-branded URL or directly through the Website/Mobile App/E Registration Form as the case may be.</p>
            <p align="justify">For availing the Service such as applying for a loan, credit card, Mutual Fund or any other financial product we will require you to provide/upload on the Website/Mobile App/E Registration Form the details such as a your name, parentage, marital status, email address, nationality, location, mobile number, PAN, employment & income details/proofs, Form 26 AS, recent photograph, signature image, other Know Your Customer (KYC) documents like address proof, identity proof and personally identifying information about a potential co-loan applicant (should you select this option), and to participate on our forum boards, a username (collectively the "Registration Information").</p>
            <p align="justify">You may opt out of location based services at any time by editing the setting of your browser.</p>
            <p align="justify">In order to provide your bank statement or pay slip electronically along with your loan application, you also must provide your third-party account credentials ("Account Credentials") to allow K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan to retrieve your account data at those other financial institutions ("Account Information") for your use. Your Account Credentials are only used once to retrieve your bank statements/pay slips, Form 26 AS and are not stored in our system. However, K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan shall not be liable to you against any liability or claims which may arise out of such transactions being carried on your own accord.</p>
            <p align="justify">We may also use third party service providers to provide the Service to you, such as sending e-mail messages on our behalf or hosting and operating a particular feature or functionality of the Service. Our contracts with these third parties outline the appropriate use and handling of your information and prohibit them from using any of your Personal Information for purposes unrelated to the product or service they re providing. We require such third parties to maintain the confidentiality of the information provide to them.</p>
            <h4>7. DISCLOSURE / SHARING:</h4>
            <p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan does not disclose sensitive personal data or Personal Information including SI of a customer except as directed by law or as per mandate received from the customer / applicant. Upon your written request K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan will provide you with information on whether we hold any of your personal information. No specific information about customer accounts or other personally identifiable data is shared with non-affiliated third parties unless any of the following conditions is met:
            <ul>
                <li>To help complete a transaction initiated by you</li>
                <li>To perform support services through a third party service provider, provided it conforms to the Privacy Policy of the K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan and your prior consent to do so is obtained</li>
                <li>You have specifically authorized it</li>
                <li>The disclosure is necessary for compliance of a legal obligation or as required by law, such as to comply with a subpoena, or similar legal process</li>
                <li>The information is shared with Government agencies mandated under law</li>
                <li>The information is shared with any third party by an order under the law</li>
                <li>When we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request.</li>
            </ul>
            </p>
            <p align="justify">There are number of products/services such as loans, credit cards, mutual funds, offered by third Parties on the Website/Mobile App/E Registration Form, such as lenders, banks, credit card issuers. If you choose to apply for these separate products or services, disclose information to these providers, then their use of your information is governed by their privacy policies. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan is not responsible for their privacy policies.</p>
            <h4>8.  INTIMATION BY CUSTOMERS REGARDING CHANGE IN REGISTRATION INFORMATION:</h4>
            <p align="justify">If your Registration Information provided to us when you had applied for a product on our Website/Mobile App/E Registration Form or Sites changes, you may update it whenever you apply for a new product via our Website/Mobile App/E Registration Form or Sites. To review and update your Personal Information and to ensure that the same is accurate while your application is in process, you may contact us at support@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com. You will not be able to update the information you have provided in an application after a decision has already been made on it; however, you may create and submit a new application with your updated information.</p>
            <p align="justify">Note: We will retain your information for as long as your account is active or as needed to provide you services. If you wish to cancel your account or request that we no longer use your information to provide you services, contact us at support@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com. We will respond to your request within a reasonable timeframe. However, we will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and to enforce our agreements.</p>
            <h4>9.  EMAIL & SMS COMMUNICATIONS FROM US AND OUR PARTNERS:</h4>
            <p align="justify">We provide our registered customers with periodic emailers and email/SMS alerts. We also allow users to subscribe to email newsletters and from time to time may transmit emails promoting K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan or third-party products. Subject to the express consent clause above, K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan’s Website/Mobile App/E Registration Form subscribers may opt-out of receiving our promotional emails and terminate their newsletter subscriptions by following the instructions in the emails. Opting out in this manner will not end transmission of service-related emails/SMS, such as email/SMS alerts. The above services are also provided by our partners.</p>
            <h4>10. LOG FILES:</h4>
            <p align="justify">This information may include internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and/or clickstream data. We may use the collected log information about you to improve services offered to you, to improve marketing, analytics, or Website/Mobile App/E Registration Form functionality.</p>
            <h4>11. TRACKING TECHNOLOGIES:</h4>
            <p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan and its partners use cookies or similar technologies to analyze trends, administer the Website/Mobile App/E Registration Form, track users’ movements around the Website/Mobile App/E Registration Form, and to gather demographic information about our user base as a whole. You can control the use of cookies at the individual browser level, but if you choose to disable cookies, it may limit your use of certain features or functions on our Website/Mobile App/E Registration Form or service. To manage Flash cookies, please visit the flash player support page.</p>
            <h4>12. BEHAVIOURAL TARGETING / RE-TARGETING:</h4>
            <p align="justify">We partner with a third party service provider to either display advertising on our Website/Mobile App/E Registration Form or to manage our advertising on other sites. Our third party partner may use technologies such as cookies to gather information about your activities on this Website/Mobile App/E Registration Form and other Sites in order to provide you advertising based upon your browsing activities and interests. If you wish to not have this information used for the purpose of serving you interest-based ads, you may opt-out by clicking here. Please note this does not opt you out of being served ads. You will continue to receive generic ads.</p>
            <h4>13. THIRD PARTIES WILL NOT BE GIVEN YOUR PERSONAL INFORMATION UNLESS YOU DIRECT K SERA SERA BOX OFFCE PVT LTD/KSS BOX OFFICE/FILMY CARAVAN TO DO SO:</h4>
            <p align="justify">There are several products and services, such as loans, credit cards, Mutual Fund, Gold Ornaments, Gold SIP and other financial products being offered by third parties on our Website/Mobile App/E Registration Form, such as lenders, banks, credit card issuers and mutual funds (collectively "Offers"). If you choose to apply for these separate products or services, disclose information to the providers, or grant them permission to collect information about you, then their use of your information is governed by their privacy policies. You should evaluate the practices of these external services providers before deciding to use their services. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan is not responsible for their privacy practices.</p>
            <h4>14. TESTIMONIALS, BLOGS AND OTHER FORUMS ON K SERA SERA BOX OFFCE PVT LTD/KSS BOX OFFICE/FILMY CARAVAN WEBSITE/MOBILE APP/E REGISTRATION FORM:</h4>
            <p align="justify">With your consent K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan may post your testimonial along with your name. If you want your testimonial removed, please contact K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan at support@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com.</p>
            <p align="justify">If you use a blog or other public forum on Website/Mobile App/E Registration Form, any information you submit there can be read, collected or used by other users and could be used to send you unsolicited messages. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan is not responsible for the Personal Information you choose to submit in these forums.</p>
            <h4>15. ADDITIONAL POLICY INFORMATION:</h4>
            <p align="justify">Widgets: Our Website/Mobile App/E Registration Form includes Widgets, which are interactive mini-programs that run on our Website/Mobile App/E Registration Form to provide specific services from another company (e.g. displaying the news, opinions, music, etc). Personal Information, such as your email address, may be collected through the Widget. Cookies may also be set by the Widget to enable it to function properly. Information collected by this Widget is governed by the privacy policy of the company that created it and not by the K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.</p>
            <p align="justify">Single Sign-On: You can log in to our Website/Mobile App/E Registration Form using sign-in services such as Facebook Connect or an Open ID provider. These services will authenticate your identity and provide you the option to share certain Personal Information with us such as your sign-in information, name and email address to link between the sites. Social networking media services like Facebook & Twitter give you the option to post information about your activities on this Website/Mobile App/E Registration Form to your profile page to share with others within your network.</p>
            <p align="justify">Like Button: If you use the "Like" button to share something that item will appear on your Facebook profile page and also on your friends’ newsfeed depending on your Facebook privacy settings. You may also receive updates in your Facebook newsfeed from this Website/Mobile App/E Registration Form or item in the future. Facebook also collects information such as which pages you have visited on this and other sites that have implemented the "Like" button.</p>
            <p align="justify">Links to 3rd Party Sites: Our Website/Mobile App/E Registration Form includes links to other Website/Mobile App/E Registration Forms whose privacy practices may differ from those of K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan. If you submit your Personal Information to any of those sites, your information is governed by their privacy policies. We encourage you to carefully read the privacy policy of any Website/Mobile App/E Registration Form you visit.</p>
            <h4>16. WE KEEP YOUR DATA SECURE:</h4>
            <p align="justify">We follow generally accepted standards to protect the Personal Information submitted to us, both during transmission and once we receive it. Since no method of transmission over the Internet, or method of electronic storage, is 100% secure, therefore, we cannot guarantee its absolute security. If you have any questions about security on our Website/Mobile App/E Registration Form, you can contact us at privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com.</p>
            <p align="justify">We use a combination of firewalls, encryption techniques and authentication procedures, among others, to maintain the security of your online session and to protect K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan online accounts and systems from unauthorized access.</p>
            <p align="justify">When you register for the Service, K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan requires a password from you for your privacy and security. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan transmits information such as your Registration Information for Website/Mobile App/E Registration Form or Account Credentials securely.</p>
            <p align="justify">Our servers are in secure facilities where access requires multiple levels of authentication, including an identity card and biometrics recognition procedures. Security personnel monitor the facilities 7 days a week, 24 hours a day.</p>
            <p align="justify">Our databases are protected from general employee access both physically and logically. We encrypt your Service password so that your password cannot be recovered, even by us. All backup drives and tapes also are encrypted.</p>
            <p align="justify">We enforce physical access controls to our buildings.</p>
            <p align="justify">No employee may put any sensitive content on any insecure machine (i.e., nothing can be taken from the database and put on an insecure laptop).</p>
            <p align="justify">K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan has been verified for its use of SSL encryption technologies. In addition, K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan tests the Website/Mobile App/E Registration Form using Acunetix scan for any failure points that would allow hacking.</p>
            <p align="justify">However, it is important to understand that these precautions apply only to our Website/Mobile App/E Registration Form and systems.</p>
            <h4>17. ALL PRIVATE INFORMATION IS ENCRYPTED AND COMMUNICATED SECURELY:</h4>
            <p align="justify">All communications between your computer and our Website/Mobile App/E Registration Form that contain any Personal Information are encrypted. This enables client and server applications to communicate in a way that is designed to prevent eavesdropping, tampering and message forgery.</p>
            <h4>18. YOU ARE RESPONSIBLE FOR MAINTAINING THE CONFIDENTIALITY OF YOUR LOGIN ID AND PASSWORD:</h4>
            <p align="justify">You are responsible for maintaining the security of your Login ID and Password, and may not provide these credentials to any third party. If you believe that they have been stolen or been made known to others, you must contact us immediately at privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com. We are not responsible if someone else accesses your account through Registration Information they have obtained from you or through a violation by you of this Privacy and Security Policy or the K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan Terms of Use.</p>
            <p align="justify">If you have any security related concern, please contact us at privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com. We will work closely with you to ensure a rapid and personal response to your concerns.</p>
            <h4>19. ISO 27001:</h4>
            <p align="justify">ISO/IEC 27001:2013 is the international standard for management of information security and provides a systematic approach to keep sensitive company information secure. Getting the ISO 27001:2013 certificate is a reassurance for our customers that K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan complies with the highest standards regarding information security. K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan is ISO/IEC 27001:2013 certified under certificate number IND17.0322/U. We have implemented the ISO/IEC 27001: 2013 standard for all processes supporting the development and delivery of services by K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.</p>
            <h4>20. CONTACT US WITH ANY QUESTIONS OR CONCERNS (GRIEVANCE REDRESSAL):</h4>
            <p align="justify">If you have grievance or complaint, questions, comments, concerns or feedback in relation to the processing of information or regarding this Privacy and Security Policy or any other privacy or security concern, send an e-mail to privacy@K Sera Sera Box Offce Pvt Ltd/KSS Box Office/Filmy Caravan.com.</p>
            <p align="justify">The name and contact details of the Grievance Officer is Harsh Upadhyay, its registered office at 101, A & 102, 1st Floor, Morya Landmark II, Andheri West, Mumbai 400053.</p>
            <h5>FEEDBACK:</h5> 
            <p align="justify">If you have an unresolved privacy or data use concern that has not been addressed satisfactorily, you are advised to contact https://feedback-form.truste.com/watchdog/request.</p>
            <h4>21. NOTICE:</h4>
            <p align="justify">The effective date of this Policy, as stated below, indicates the last time this Policy was revised or materially changed.</p>
            <h4>22. EFFECTIVE DATE:</h4>
            <p align="justify">April 1, 2018</p>';
    }


     public function education(Request $request){       
        $educations = config('custome.EDUCATIONS');
        $arredu = [];
        foreach ($educations as $key => $value) {
            $arredu[]['EDUCATIONS'] = $value;
        }        
        return ['success'=>true,'error'=>'','data'=>['Education'=>$arredu]];         
    }

    public function employment(Request $request) {
        $educations = config('custome.EMPLOYMENT');
        $arr=[];
        foreach ($educations as $key => $value) {
            $arr[]['EMPLOYMENT'] = $value;
        }       
        return ['success'=>true,'error'=>'','data'=>['employmentdata'=>$arr]];
      
    }

    public function vehicle(Request $request) {
        $educations = config('custome.VEHICLE');
        $arrv=[];
        foreach ($educations as $key => $value) {
          $arrv[]['VEHICLE'] = $value;
        }         
          return ['success'=>true,'error'=>'','data'=>['vehicledata'=>$arrv]];
    }

    public function anylongterm (Request $request) {
        $educations = config('custome.ANYLONGTERM');
        $arrany =[];     
       // dd($educations); 
        foreach ($educations as $key => $value) {
                   $arrany[$key]['ANYLONG'] = $value  ;
                    $arrany[$key]['id'] = $key+1;                

                 }                      
        return ['success'=>true,'error'=>'','data'=>['anylongterm'=>$arrany]];

    }

    public function landproperty(Request $request) {
       $educations = config('custome.LANDPROPERTY');  
       $arrland = [];
        foreach ($educations as $key => $value) {
               $arrland[]['LANDPROPERTY'] = $value;
           }       
          return ['success'=>true,'error'=>'','data'=>['Landpropertydata'=>$arrland]];

         }

         public function existingloan(Request $request) {
             $educations = config('custome.ExistingLoan');
              $arrloan = [];
               foreach ($educations as $key => $value) {
                $arrloan[]['Loan'] = $value;
               }
               return ['success'=>true,'error'=>'','data'=>['ExistingLoan'=>$arrloan]];
         }        

         public function insurance(Request $request) {
             $educations = config('custome.Insurance');
              $arrinsurance = [];
               foreach ($educations as $key => $value) {
                $arrinsurance[]['Insurance'] = $value;
               }
               return ['success'=>true,'error'=>'','data'=>['insurancedata'=>$arrinsurance]];
         }


         public function userupdateprofile(Request $request)
          {
            

                    $id     =    $request['id'];
                   $name   =    $request['name'];
                   $mobile =    $request['mobile'];
                   $email  =    $request['email'];
                   $addharno  =    $request['aadhar_number'];               
                   
                   User::where (["id" =>$request['id']])
                   ->update([
                   'name' => $request['name'],
                   'mobile' => $request['mobile'],
                   'email' => $request['email'],                 
                    'aadhar_number' =>$request['aadhar_number'], 
                   ]);

                   return ['success'=>true,'error'=>'','data'=> 
                   ['id'=>$id,'name'=>$name,'mobile'=>$mobile,'email'=>$email,'aadhar_number'=>$addharno]];

                           

                         
          }

       public function getuserprofile(Request $request) {      
             
                    $users = DB::table('users')
                               ->where("id", '=',$id) 
                             ->select('*')
                             ->get();
                     foreach ($users as $key => $value) {
                        $value[];
                     }
                   return ['success'=>true,'error'=>'','data'=>['id'=>$value->id,'name'=>$value->name,'email'=>$value->email,
                 'mobile'=>$value->mobile,'addharno'=>$value->aadhar_number]];

                    
               }





}