<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Otp;
use Illuminate\Http\Request;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'mobile'=>$data['mobile'],
            'password' => bcrypt($data['password']),
            'aadhar_number'=>$data['aadhar_number']
        ]);
    }

    public function memberregister() {
        return view('auth/register');
    }

    public function register(Request $request) {
        $otp = $request['otp'];           
        $otpcheck = Otp::where('otp', $otp)
                        ->select("otp")
                        ->first();                             
        if(empty($otpcheck)){
            return ['success'=>false,'error'=>'Otp Is Incorrect, Please Re-enter Otp','data'=>[]];        
        }
        $registerObj = $this->create($request->all());
        Auth::loginUsingId($registerObj->id);
        return ['success'=>true,'error'=>'','data'=>1];
    }

    public function otp(Request $request){
        $mobile = $request['mobile'];
        //$email = $request['email'];
        $data = User::where('mobile',$mobile)
                    ->select("mobile")
                    ->first();
        if(!empty($data)){
            return ['success'=>false,'error'=>'Mobile or Email Already Exist!!!','data'=>[]];
        }
        $num_str = sprintf("%06d",mt_rand(1,999999));    
        $message="$num_str is the otp for filmy caravan";
        $this->sendsms($mobile,$message);
        $opt = Otp::where(['number'=>$request['mobile']]);
        $check = clone ($opt);
        if(!empty($opt->first())){
            return $check->update(['otp'=>$num_str]);
        }
        $otpObj = new Otp();
        $otpObj->number = $request['mobile'];
        $otpObj->otp = $num_str;
        $otpObj->save();        
        return 1;       
    }
}
