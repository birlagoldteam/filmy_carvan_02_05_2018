<?php

namespace App\Http\Controllers;

use App\Memberships;
use App\FamilyMembers;
use App\Categories;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Session;

class MembershipsController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function membershipRegistration() {
		$membership = Memberships::where('user_id', Auth::user()->id)->first();
		$categories = Categories::with('subCategories')->get();
		$state = $this->getStates();
        return view('memberships/add-membership', ['membership' => $membership,'categories'=>$categories,'state'=>$state]);
    }
    
    public function membershipCreate(Request $request) {
    	//dd($request);
		$membership = new Memberships;
		$membership['user_id'] = Auth::user()->id;
		$membership['name'] = $request['name'];
		$membership['email'] = $request['email'];
		$membership['mobile'] = $request['mobile'];
		$membership['gender'] = $request['gender'];
		$membership['gender_other'] = $request['gender_other'];
		$membership['date_of_birth'] = !empty($request['date_of_birth']) ? date('Y-m-d',strtotime($request['date_of_birth'])) : null;
		$membership['aadhar_number'] = $request['aadhar_number'];
		$membership['pan'] = $request['pan'];
		$membership['marital_status'] = $request['marital_status'];
		$membership['children'] = $request['children'];
		$membership['blood_group'] = 1;//$request['blood_group'];
		$membership['country'] = $request['country'];
		$membership['state'] = $request['state'];
		$membership['city'] = $request['city'];
		$membership['tehsil'] = $request['tehsil'];
		$membership['grampanchayat'] = $request['grampanchayat'];
		$membership['village'] = $request['village'];
		$membership['street_area'] = $request['street_area'];
		$membership['house_name_no'] = $request['house_name_no'];
		$membership['pincode'] = $request['pincode'];
		$membership['education'] = $request['education'];
		$membership['other_education'] = $request['other_education'];
		$membership['language_known'] = $request['language_known'];
		$membership['employment'] = $request['employment'];
		$membership['employment_other'] = $request['employment_other'];
		$membership['handicapped'] = $request['handicapped'];
		$membership['long_term_diseases'] = $request['long_term_diseases'];
		$membership['social_needs_for_me'] = $request['social_needs_for_me'];
		$arr = array();
		//dd($request->all());
		foreach($request->family_name as $key=>$value){
			$arr[$key]['name'] = $value;
			$arr[$key]['age'] = $request->family_age[$key];
			$arr[$key]['relationship'] = $request->family_relationship[$key];
			$arr[$key]['employment'] = $request->family_employment[$key];
			$arr[$key]['mobile'] = $request->family_mobile[$key];
			$arr[$key]['social_needs'] = $request->family_social_needs[$key];
		}
		$arr[count($request->family_name)]['name'] = $request['name'];
		$dob = !empty($request['date_of_birth']) ? date('Y-m-d',strtotime($request['date_of_birth'])) : null;
		if(!empty($request['date_of_birth'])) {
			$diff = (date('Y') - date('Y',strtotime($dob)));
		} else {
			$diff = null;
		}
		$arr[count($request->family_name)]['age'] =  $diff;
		$arr[count($request->family_name)]['relationship'] = 'Self';
		$arr[count($request->family_name)]['employment'] = $request['employment'];
		$arr[count($request->family_name)]['mobile'] = $request['mobile'];
		$arr[count($request->family_name)]['social_needs'] = $request['social_needs_for_me'];
		
		if(count($arr)>0){
			FamilyMembers::where('user_id', Auth::user()->id)->delete();
            $replace = array_fill(0,count($arr),['user_id'=>Auth::user()->id]);
            FamilyMembers::insert(array_replace_recursive($arr,$replace));
        }
		
		$membership['family_annual_income'] = $request['family_annual_income'];
		$membership['monthly_expenses'] = $request['monthly_expenses'];
		$membership['property_ownership'] = $request['property_ownership'];
		$membership['property_type'] = $request['property_type'];
		$membership['commercial'] = $request['commercial'];
		$membership['other_property'] = $request['other_property'];
		$membership['property_ownership'] = $request['property_ownership'];
		$membership['property_type'] = $request['property_type'];
		$membership['commercial'] = $request['commercial'];
		$membership['other_property'] = $request['other_property'];
		
		$membership['vehicle'] = $request['vehicle'];
		$membership['other_vehicle'] = $request['other_vehicle'];
		
		$membership['loan'] = $request['loan'];
		$membership['loan_type'] = $request['loan_type'];
		$membership['other_loan'] = $request['other_loan'];
		$membership['insurance'] = $request['insurance'];
		$membership['insurance_type'] = $request['insurance_type'];
		$membership['insured_company_name'] = $request['insured_company_name'];
		$membership['other_insurance'] = $request['other_insurance'];
		$membership['platform'] = 'Web';
		
		//dd($membership);
		$membership->save();
		return Redirect::to('membership-registration');
    }
    
    public function membershipUpdation() {
		$user = Memberships::with('familyMembers')->where('user_id',Auth::user()->id)->first();
		//dd(DB::getQueryLog());
		//~ dd($user->familyMembers->All());
		//dd($user);
		//$user->family_members = json_decode($user->family_members);
		$categories = Categories::with('subCategories')->get();
		$state = $this->getStates();
		//dd($categories);
        return view('memberships/update-membership', ['user' => $user,'categories'=>$categories,'state'=>$state]);
    }
    
    public function membershipUpdate(Request $request) {
		$user = Memberships::where('user_id', Auth::user()->id)->first();
		//dd($request);
		$user['gender'] = $request['gender'];
		$user['gender_other'] = $request['gender_other'];
		//$user['date_of_birth'] = $request['date_of_birth'];
		$user['date_of_birth'] = !empty($request['date_of_birth']) ? date('Y-m-d',strtotime($request['date_of_birth'])) : null;
		$user['aadhar_number'] = $request['aadhar_number'];
		//$user['pan'] = $request['pan'];
		$user['marital_status'] = $request['marital_status'];
		//$user['children'] = $request['children'];
		$user['blood_group'] = 1;//$request['blood_group'];
		$user['country'] = $request['country'];
		$user['state'] = $request['state'];
		$user['city'] = $request['city'];
		$user['tehsil'] = $request['tehsil'];
		$user['grampanchayat'] = $request['grampanchayat'];
		$user['village'] = $request['village'];
		$user['street_area'] = $request['street_area'];
		$user['house_name_no'] = $request['house_name_no'];
		$user['pincode'] = $request['pincode'];
		$user['education'] = $request['education'];
		$user['other_education'] = $request['other_education'];
		$user['language_known'] = $request['language_known'];
		$user['employment'] = $request['employment'];
		$user['employment_other'] = $request['employment_other'];
		$user['handicapped'] = $request['handicapped'];
		$user['long_term_diseases'] = $request['long_term_diseases'];
		$user['social_needs_for_me'] = $request['social_needs_for_me'];
		$arr = array();
		if(!empty($request->family_name)) {
			foreach($request->family_name as $key=>$value){
				$arr[$key]['name'] = $value;
				$arr[$key]['age'] = $request->family_age[$key];
				$arr[$key]['relationship'] = $request->family_relationship[$key];
				$arr[$key]['employment'] = $request->family_employment[$key];
				$arr[$key]['mobile'] = $request->family_mobile[$key];
				$arr[$key]['social_needs'] = $request->family_social_needs[$key];
			}
		}
		
		$arr[count($request->family_name)]['name'] = $request['name'];
		$dob = !empty($request['date_of_birth']) ? date('Y-m-d',strtotime($request['date_of_birth'])) : null;
		if(!empty($request['date_of_birth'])) {
			$diff = (date('Y') - date('Y',strtotime($dob)));
		} else {
			$diff = null;
		}
		$arr[count($request->family_name)]['age'] =  $diff;
		$arr[count($request->family_name)]['relationship'] = 'Self';
		$arr[count($request->family_name)]['employment'] = $request['employment'];
		$arr[count($request->family_name)]['mobile'] = $request['mobile'];
		$arr[count($request->family_name)]['social_needs'] = $request['social_needs_for_me'];
		
        if(count($arr)>0){
			FamilyMembers::where('user_id', Auth::user()->id)->delete();
            $replace = array_fill(0,count($arr),['user_id'=>Auth::user()->id]);
            FamilyMembers::insert(array_replace_recursive($arr,$replace));
        }
		
		//$user['family_members'] = json_encode($arr);
		$user['type_of_work'] = $request['type_of_work'];
		$user['other_type_of_work'] = $request['other_type_of_work'];
		$user['income'] = $request['income'];
		
		$user['family_annual_income'] = $request['family_annual_income'];
		$user['monthly_expenses'] = $request['monthly_expenses'];
		$user['property_ownership'] = $request['property_ownership'];
		$user['property_type'] = $request['property_type'];
		$user['commercial'] = $request['commercial'];
		$user['other_property'] = $request['other_property'];
		$user['property_ownership'] = $request['property_ownership'];
		$user['property_type'] = $request['property_type'];
		$user['commercial'] = $request['commercial'];
		$user['other_property'] = $request['other_property'];
		
		$user['vehicle'] = $request['vehicle'];
		$user['other_vehicle'] = $request['other_vehicle'];
		
		$user['loan'] = $request['loan'];
		$user['loan_type'] = $request['loan_type'];
		$user['other_loan'] = $request['other_loan'];
		$user['insurance'] = $request['insurance'];
		$user['insurance_type'] = $request['insurance_type'];
		$user['insured_company_name'] = $request['insured_company_name'];
		$user['other_insurance'] = $request['other_insurance'];
		
		$user->save();
		$user->family_members = json_decode($user->family_members);
		//return view('memberships/add-membership', ['user' => $user]);
		return Redirect::to('membership-registration');
    }
    
    public function getDistricts(Request $request) {
    	$stateId = $request->state_id;
    	return static::getDistrict($stateId);
    }

    public function getTehsils(Request $request) {
    	$districtId = $request->district_id;
    	return static::getTehsil($districtId);
    }

    public function getgrampanchayats(Request $request) {
    	$tehsilId = $request->tehsil_id;
    	return static::getgrampanchayat($tehsilId);
    }
}
