<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NgoDetails;
use App\NgoMembers;
use App\Http\Controllers\Controller;
use Mail;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
// use Symfony\Component\HttpFoundation\File\UploadedFile;


// use Illuminate\Support\Facades\Input;
// use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Collection;




class NgoDetailsController extends Controller
{
    //
	public function addNgo() {
	 $state = $this->getStates();

	return view('ngo/addngodetails', ['state'=>$state]);
	
	}
	
	
	public function add_ngo(Request $request) {
		
		$ngo_details = new NgoDetails;
		$ngo_details->unique_id = $request->unique_id;
 		$ngo_details->name = $request->name;
 		$ngo_details->email = $request->email;
 		$ngo_details->address = $request->address;
		$ngo_details->state = $request->state;
		$ngo_details->city = $request->city;
		$ngo_details->telephone = $request->telephone;
		$ngo_details->mobile = $request->mobile;
		$ngo_details->url = $request->url;
		$ngo_details->registered_with = $request->registered_with;
		$ngo_details->ngo_type = $request->ngo_type;
		$ngo_details->registration_no = $request->registration_no;
		$ngo_details->registration_certificate = $request->registration_certificate;
	    $ngo_details->registration_name = $request->file('registration_name')->getClientOriginalName();
	    $ngo_details->registration_dir = '/public/ngo/Registration_copy/';

        // $ngo_details->registration_dir = $request->file('registration_name')->
        //    getRealPath();

        $reg = $request->file('registration_name');
	    //Move Uploaded File
	    $destinationPath = storage_path('app/public/ngo/Registration_copy/'.'1');
	    $reg->move($destinationPath,$reg->getClientOriginalName());  
	 	
	    // return $ngo_details->registration_dir = $request->file('registration_name')->
	    //  getRealPath();

		$ngo_details->pan = $request->pan;
		$ngo_details->pan_name = $request->file('pan_name')->getClientOriginalName();
		$ngo_details->pan_dir = '/public/ngo/pan_copy/';
	    $destinationPath = storage_path('app/public/ngo/Pan_copy/'.'1');
	    $pan= $request->file('pan_name');
	    $pan->move($destinationPath,$pan->getClientOriginalName());  
	      
		$ngo_details->act_name = $request->act_name;
		$ngo_details->ngo_register_state = $request->ngo_register_state;
		$ngo_details->ngo_register_city = $request->ngo_register_city;
		$ngo_details->volunteer_no = $request->volunteer_no;
		$ngo_details->fund_status = $request->fund_status;
		$ngo_details->registration_date = $request->reg_date;
		$ngo_details->key_issues = $request->key_issues;
		$ngo_details->operation_area_state = $request->operation_area_state;
		$ngo_details->operation_area_city = $request->operation_area_city;
		$ngo_details->fcra = $request->fcra;
		$ngo_details->fcra_reg_no = $request->fcrano;
		$ngo_details->skill_development = $request->skill;
    
		$arr = array();

		foreach($request->member_name as $key=>$value){
        //return($request->member_profile_upload[$key]);
			$arr[$key]['name'] = $value;
			$arr[$key]['designation'] = $request->member_designation[$key];
			$arr[$key]['pan'] = $request->member_pan[$key];
			$arr[$key]['aadhar'] = $request->member_aadhar[$key];
			$arr[$key]['email'] = $request->member_email[$key];
			$arr[$key]['mobile'] = $request->member_mobile[$key];

      		$arr[$key]['profile'] = $request->member_profile_upload[$key]->getClientOriginalName();

      		$arr[$key]['profile_dir'] = '/public/ngo/members/profile/';

      		// $arr[$key]['profile_dir'] = $request->member_profile_upload[$key]->getRealPath();

	        $destinationPath = storage_path('app/public/ngo/members/profile/'.'1');
	        $profile= $request->member_profile_upload[$key];
	        $profile->move($destinationPath,$profile->getClientOriginalName()); 

	        // $arr[$key]['profile_dir'] = $request->member_profile_upload[$key]->getRealPath();


		}
		 if(count($arr)>0){
			NgoMembers::where('user_id', 1)->delete();
            $replace = array_fill(0,count($arr),['user_id'=>1]);
            NgoMembers::insert(array_replace_recursive($arr,$replace));
        }
		$ngo_details->save();
		return Redirect::to('ngo-add-details');
   	}

}

