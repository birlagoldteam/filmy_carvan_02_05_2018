<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Controllers\SendSmsController;
use App\State;
use App\District;
use App\Tehsil;
use App\Grampanchayat;
use Mail;
use App\Otp;

class Controller extends BaseController{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendsms($mobile,$message){
		$sms_site=env('SMS_SITE');
		$sms_method=env('SMS_METHOD');
		$sms_api_key=env('SMS_API_KEY');
		$sms_sender_id=env('SMS_SENDER_ID');
		$sendsms = new SendSmsController($sms_site,$sms_method,$sms_api_key,$sms_sender_id);
        $sendsms->send_sms($mobile,$message,'','xml');
	}

	public static function getStates() {
		return State::select()->get();
	}

	public static function getDistrict($stateId=null) {
		$obj = District::select();
		if(!empty($stateId)) {
			$obj = $obj->where('state_id',$stateId);
		}
		return $obj->get();

	}

	public static function getTehsil($districtId=null) {
		$obj = Tehsil::select();
		if(!empty($districtId)) {
			$obj = $obj->where('district_id',$districtId);
		}
		return $obj->get();

	}

	public static function getgrampanchayat($tehsil=null) {
		$obj = Grampanchayat::select();
		if(!empty($tehsil)) {
			$obj = $obj->where('tahsil_id',$tehsil);
		}
		return $obj->get();

	}
public function sendemail($data){
        Mail::send(['html' => 'email.layout'],['content'=>$data['messages']],function($message) use($data){
            $message->to($data['to_mail'],$data['to_name']);
            $message->from($data['from_email'],$data['from_name']);
            $message->subject($data['subject']);
            if(array_key_exists('attachfile',$data)){
                foreach($data['attachfile'] as $file){
                    $message->attach($file);
                }   
            }
        });
    }

 public function opt(array $request){
		$key = isset($request['email']) ? "email" : "number";
		$value = $key === "email" ? $request['email'] : $request['mobile'];
		$opt = Otp::where([$key=>$value]);
    	$check = clone ($opt);
        if(!empty($opt->first())){
            return $check->update(['otp'=>$request['otp']]);
        }
        $otpObj = new Otp();
        $otpObj->$key = $value;
        $otpObj->otp = $request['otp'];
        $otpObj->save();
        return $otpObj;
    }
}
