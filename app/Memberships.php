<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Memberships extends Model
{
	protected $table = 'memberships';
	
	protected $primaryKey = 'id';
	
	public function familyMembers() {
		return $this->hasMany('App\FamilyMembers', 'user_id', 'user_id');
	}

}
