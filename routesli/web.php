<?php

Auth::routes();
Route::get('/', function () {	
	return view('welcome');
})->name('welcome');

Route::get('/', function () {
	return view('welcome');
})->name('welcome');

Route::get('about-us', function () {
	return view('website.about');
})->name('about');

Route::get('what-we-do', function () {
	return view('website.whatwedo');
})->name('whatwedo');

Route::get('management-team', function () {
	return view('website.managementteam');
})->name('managementteam');

Route::get('production', function () {
	return view('website.production');
})->name('production');

Route::get('distribution', function () {
	return view('website.distribution');
})->name('distribution');

Route::get('film-exhibition', function () {
	return view('website.filmexhibition');
})->name('filmexhibition');

Route::get('filmy-caravan', function () {
	return view('website.filmycaravan');
})->name('filmycaravan');

Route::get('opportunity', function () {
	return view('website.opportunity');
})->name('opportunity');

Route::get('contact-us', function () {
	return view('website.contact');
})->name('contact');


Route::post('search-film',function () {
	return view('website.movies.moviesschedule');
})->name('getfilm');


/*Route::get('member-registration', function () {
	return view('member.memberregister');
})->name('memberregister');


*/



Route::get('/home', 'HomeController@index')->name('home');

Route ::get('member-register',[
	'uses'=> 'Auth\RegisterController@memberregister',
	'as'=>  'memberregister',
]);

Route::post('check-otp',[
	'uses' =>'Auth\RegisterController@otp', 
	'as' =>'checkotp',
]);

Route::post('member-register',[
	'uses' =>'Auth\RegisterController@register', 	
	'as' =>'member-register-post',
]);


Route::get('membership-registration',[
	'uses'=>'MembershipsController@membershipRegistration',
	'as'=>'membershipregistration',
	'middleware'=>'auth'
]);


Route::post('membership-create', [
    'uses' => 'MembershipsController@membershipCreate',
    'as' => 'membership-create',
]);

Route::get('get-district', [
    'uses' => 'MembershipsController@getDistricts',
    'as' => 'getdistrict',
    //'middleware'=>'auth'
]);

Route::get('get-tehsil', [
    'uses' => 'MembershipsController@getTehsils',
    'as' => 'gettehsil',
    //'middleware'=>'auth'
]);

Route::get('get-grampanchayat', [
    'uses' => 'MembershipsController@getgrampanchayats',
    'as' => 'getgrampanchayat',
    //'middleware'=>'auth'
]);

Route::get('membership-updation','MembershipsController@membershipUpdation')->name('membership-updation')->middleware('auth');
Route::post('membership-update', [
	'uses' => 'MembershipsController@membershipUpdate',
	'as' => 'membership-update',
	'middleware'=>'auth'
]);

Route::get('ngo-registration', [
	'uses' => 'NgoHomeController@demo',
	'as' => 'ngo-registration',	
]);

Route::post('otpngoverify', [
	'uses' => 'NgoHomeController@otpngoverify',
	'as' => 'otpngoverify',
	//'middleware'=>'auth'
	
]);


Route::post('emailvalidate', [
	'uses' => 'NgoHomeController@emailvalidatengo',
	'as' => 'emailvalidate',
	
]);

Route::post('addngo-register', [
	'uses' => 'NgoHomeController@addngoregister',
	'as' => 'addngo-register',
	
]);





Route::get('ngo-dashboard', function () {
	return view('ngo.ngodashboard');
})->name('ngodashboard');

//~ Route::get('ngo-add-details', function () {
	//~ return view('ngo.addngodetails');
//~ })->name('addngodetails');

Route::get('ngo-add-details',[
	'uses'=>'NgoDetailsController@addNgo',
	'as'=>'addngodetails',
	//'middleware'=>'auth'
]);

Route::post('add-ngo', [
    'uses' => 'NgoDetailsController@add_ngo',
    'as' => 'add_ngo'
    // 'middleware'=>'auth'
]);



Route::group(['prefix' => 'api'], function () {
    Route::post('/generateotp','TestController@generateotp');
    Route::post('/register', 'TestController@register');
    Route::post('/memberrship-register', 'TestController@membership_register');
    Route::post('/social-issue', 'TestController@social_issue');
    Route::post('/state', 'TestController@state');
    Route::post('/district', 'TestController@district');
    Route::post('/tehsil', 'TestController@tehsil');
    Route::post('/grampanchayat', 'TestController@grampanchayat');
    Route::post('/terms-conditions', 'TestController@terms_conditions');
    Route::post('/education','TestController@education');
    Route::post('/employment','TestController@employment');
    Route::post('/vehicle','TestController@vehicle');
    Route::post('/anylongterm','TestController@anylongterm');
    Route::post('/landproperty','TestController@landproperty');
    Route::post('/existingloan','TestController@existingloan');
    Route::post('/insurance','TestController@insurance');
    Route::post('/userupdatepro','TestController@userupdateprofile'); 
    Route::get('/getuserprofile','TestController@getuserprofile'); 

});

Route::group(['prefix'=>'ngo'], function () {
	Route::get('/login', 'Auth\NgoLoginController@showloginform')->name('ngo-login');
	Route::post('/login', 'Auth\NgoLoginController@login')->name('ngo-login-submit');
	Route::get('/', 'NgoHomeController@index')->name('ngohome');
});

Route::get('check',function(Illuminate\Http\Request $request){
	dd(Auth::guard('ngo')->user()->name);
	
});

Route::post('demo', 'ContactUsController@cont')->name('demo');
