@extends('layouts.app')
@section('page_title')
    NGO Registration
@endsection

@section('content')
<section class="page-header page-header-light page-header-more-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>NGO Registration</h1>
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li>Home</a></li>
                    <li class="active">Register</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-12" align="center">
            <div class="featured-boxes">
                <div class="row">
                    <div class="col-sm-1"></div> 
                    <div class="col-sm-10">
                        <div class="featured-box featured-box-primary align-left mt-xlg">
                            <div class="box-content">
                                <h4 class="heading-primary text-uppercase mb-md">Register A NGO</h4>                               
                                     <form id="ngoregister" name="ngoregister" action="{{ url('addngo-register') }}" class="form-horizontal form-bordered" method="post">
                                     <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">                                           
                                        
                                                <label>Name</label>
                                                <input type="text" value="" class="form-control input-lg" name="name" id="name">                                                                                     
                                        </div>
                                          
                                         <div class="form-group col-md-6">
                                                <label>E-mail Address</label>
                                                <input type="text" value="" class="form-control input-lg" name="email" id="email" onblur="check_if_exists();">
                                            </div>
                                         
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <label>Mobile No.</label>
                                                <input type="text" value="" class="form-control input-lg" name="mobile" id="mobile">
                                            </div>
                                             <div class="col-md-4">
                                                <label>Password</label>
                                                <input type="password" value="" class="form-control input-lg" name="password" id="password">
                                            </div>
                                            <div class="col-md-4">
                                                <label>Re-enter Password</label>
                                                <input type="password" value="" class="form-control input-lg" name="repassword" id="repassword">
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="row">
                                           
                                            <div align="center" class="col-md-12">                                                
                                                 <button type="submit" class="btn btn-primary mt-xl" id="registerngo">Register</button>
                                            </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div> 
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('page_level_script_bottom')
    <script src="{{URL::asset('public/js/validation/jquery.validate.js')}}"></script>
    <script src="{{URL::asset('public/js/validation/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/js/validation/app.js')}}"></script>
    <script>
        jQuery(document).ready(function(){
            App.init();
            FormValidation.init();
        });       

       $('#registerngo').click(function(){  
                var name = $('#name').val();
    	        var email = $('#email').val();
    	 	var mobile = $('#mobile').val();    	 	
    	 	var password = $('#password').val();
    	 	var token = $('#token').val();                  
                $.ajax({
                url: "{{ route('addngo-register')}}",
                data:{'name':name,'email':email,'mobile':mobile,'password':password,'_token': token},
                type: 'post',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                     if(response['success']===false){
                        alert(response['error']);                            
                    }else{                        
                    }
                    },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
    	 	
            
        });
        
  function check_if_exists() {
  var email = $('#email').val();
  var token = $('#token').val();
 
     $.ajax({
                url: "{{ route('emailvalidate')}}",
                data:{'email':email,'_token': token},
                type: 'post',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                    if(response['success']===false){
                        alert(response['error']);                            
                    }else{                        
                    }
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
  
  
   

}

    </script>
@endsection 