@extends('layouts.app')
@section('page_title')
    Members
@endsection
@section('content')
   <section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Dahsboard</h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">Dashboard</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="agent-item">
            <div class="row">
                <!-- <div class="col-md-2">
                    <img src="img/team/team-11.jpg" class="img-responsive" alt="">
                </div> -->
                <div class="col-md-6">
                    <h3>Being Human</h3>
                    <!-- <h6 class="mb-xs">ssss</h6> -->
                    <h5 class="mt-xs mb-xs">Click <i class="fa fa-hand-o-down"></i> for Add Products </h5>
                    <a class="btn btn-secondary btn-sm mt-md" href="#addproduct">Add Product</a>
                </div>
                <div class="col-md-4">
                    <ul class="list list-icons m-sm ml-xl">
                        <li>
                            <a href="mailto: mail@domain.com">
                                <i class="icon-envelope-open icons"></i> sukhsagar@domain.com
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-call-out icons"></i> (800) 123-4567
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-social-linkedin icons"></i> Lindekin
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-social-facebook icons"></i> Facebook
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div id="addproduct" class="heading heading-border heading-middle-border heading-middle-border-center">
            <h2>Add Products</h2>
        </div>

        <!-- <div id="membership" class="row">
            <h2 align="center">Filmy Caravan Membership Registration</h2>
        </div> -->

        <div class="row">
            <div class="col-md-12">
                <div class="agent-item agent-item-detail">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="contactForm" action="php/contact-form.php" method="POST">
                                <input type="hidden" value="Contact Form" name="subject" id="subject">
                                
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Name *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Category *</label>
                                            <select class="form-control ">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Sub Category *</label>
                                            <select class="form-control ">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>State *</label>
                                            <select class="form-control ">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>City/District *</label>
                                            <select class="form-control ">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Tehsil *</label>
                                            <select class="form-control ">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label>Gram Panchayat*</label>
                                            <select class="form-control ">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Village *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                                        </div>
                                    </div>

                                    <div class="post-meta">
                                        <span style="float: right;" class="d-block d-sm-inline-block float-sm-right mt-3 mt-sm-0"><a href="#" class="btn btn-xs btn-primary">Add More +</a></span>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="submit" value="Submit" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection