<footer id="footer" class="m-none custom-background-color-1">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h4 class="mb-md">About Us</h4>
				<nav class="nav-footer">
					<ul class="custom-list-style-1 pl-lg">
						<li>
							<a href="{{route('about')}}" class="custom-color-2 text-decoration-none">
								Company Profile
							</a>
						</li>
						<li>
							<a href="{{route('whatwedo')}}" class="custom-color-2 text-decoration-none">
								What we do
							</a>
						</li>
						<li>
							<a href="{{route('managementteam')}}" class="custom-color-2 text-decoration-none">
								Management Team
							</a>
						</li>
						<li>
							<a href="#" class="custom-color-2 text-decoration-none">
								Vision Mission & Values
							</a>
						</li>
						<li>
							<a href="#" class="custom-color-2 text-decoration-none">
								Business Vertical
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="col-md-4">
				<h4 class="mb-md">Contact Us</h4>
				<p class="custom-color-2">
					315, Crystal Point Mall, New Link Road,<br>
					Andheri (West), Mumbai MH. 400053 <br>
					Phone : 123-456-7890<br>
					Email : <a class="text-color-secondary" href="mailto:info@kssboxoffice.com">info@kssboxoffice.com</a>
				</p>
			</div>
			<!-- <div class="col-md-3">
				<h4 class="mb-md">K Sera Sera - Box Office</h4>
				<p class="custom-color-2">
					315, Crystal Point Mall, New Link Road,<br>
					Andheri (West), Mumbai MH. 400053 <br>
					Phone : 123-456-7890<br>
					Email : <a class="text-color-secondary" href="mailto:info@kssboxoffice.com">info@kssboxoffice.com</a>
				</p>
			</div> -->
			<!-- <div class="col-md-3">
				<h4 class="mb-md">Business Vertical</h4>
				<nav class="nav-footer">
					<ul class="custom-list-style-1 pl-lg">
						<li>
							<a href="kss-box-office-production.php" class="custom-color-2 text-decoration-none">
								Production
							</a>
						</li>
						<li>
							<a href="kss-box-office-distribution.php" class="custom-color-2 text-decoration-none">
								Distribution
							</a>
						</li>
						<li>
							<a href="kss-box-office-film-exhibition.php" class="custom-color-2 text-decoration-none">
								Film Exhibition
							</a>
						</li>
					</ul>
				</nav>
			</div> -->
			<!-- <div class="col-md-2">
				<h4 class="mb-md">About Us</h4>
				<nav class="nav-footer">
					<ul class="custom-list-style-1 pl-lg">
						<li>
							<a href="demo-real-estate-agents.html" class="custom-color-2 text-decoration-none">
								Company Profile
							</a>
						</li>
						<li>
							<a href="demo-real-estate-who-we-are.html" class="custom-color-2 text-decoration-none">
								What we do
							</a>
						</li>
						<li>
							<a href="demo-real-estate-contact.html" class="custom-color-2 text-decoration-none">
								Management Team
							</a>
						</li>
						<li>
							<a href="demo-real-estate-contact.html" class="custom-color-2 text-decoration-none">
								Contact
							</a>
						</li>
					</ul>
				</nav>
			</div> -->
			<!-- <div class="col-md-4">
				<h4 class="mb-md">Social Connects</h4>
				<div id="tweet" class="twitter" data-plugin-tweets data-plugin-options="{'username': '', 'count': 1}">
					<p>Please wait...</p>
				</div>
			</div> -->

			<div class="col-md-4">
				<h4>Social Connects</h4>
				<ul class="social-icons">
					<li class="social-icons-facebook"><a href="https://www.facebook.com/Filmy-Caravan-1571539446305340/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
					<li class="social-icons-twitter"><a href="https://twitter.com/filmycaravan" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
					<li class="social-icons-facebook"><a href="https://instagram.com/filmycaravan" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
					<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright custom-background-color-1 pb-none">
		<div class="container">
			<div class="row pt-md pb-md">
				<div class="col-md-12 center m-none">
					<p>© Copyright 2018. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>