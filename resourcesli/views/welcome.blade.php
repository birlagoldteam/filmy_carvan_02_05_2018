@extends('layouts.app')
@section('page_title')
    Title Deepak
@endsection
@section('content')
    <div class="slider-container rev_slider_wrapper" style="height: 650px;">
    <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 550, 'disableProgressBar': 'on'}">
        <ul>
            <li data-transition="fade">
                <img src="{{URL::asset('public/img/demos/real-estate/slides/Filmy_Carvan_Home_Image.jpg')}}"  
                    alt=""
                    data-bgposition="center center" 
                    data-bgfit="cover" 
                    data-bgrepeat="no-repeat"
                    class="rev-slidebg">
            </li>
            <li data-transition="fade">
                <img src="{{URL::asset('public/img/demos/real-estate/slides/filmy-caravan-Home-Page Banner-2.jpg')}}"  
                    alt=""
                    data-bgposition="center center" 
                    data-bgfit="cover" 
                    data-bgrepeat="no-repeat"
                    class="rev-slidebg">
            </li>
        </ul>
    </div>
    </div>
   <section class="section section-tertiary section-front mt-none" style="margin:0 !important;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 center">
                    <div class="row">
                        <div class="col-md-12 center">
                            <h2 class="font-weight-normal mb-xs">
                            K Sera Sera <strong style="color: #e94b1c !important;" class="text-color-secondary font-weight-extra-bold">Box Office</strong>
                            </h2>
                            <div class="divider divider-primary divider-small divider-small-center mb-xl">
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="counters with-borders">
                            <div class="col-md-4 col-sm-8">
                                <div class="counter counter-primary" style="background-color: #fff;">
                                    <!-- <strong data-to="100" data-append="+">0</strong>
                                    <label>Films Produced</label> -->
                                    <strong data-to="100">0</strong>
                                    <label>Films Produced</label>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-8">
                                <div class="counter counter-primary" style="background-color: #fff;">
                                    <strong data-to="100">0</strong>
                                    <label>Films Distributed</label>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-8">
                                <div class="counter counter-primary" style="background-color: #fff;">
                                    <strong data-to="3000">0</strong>
                                    <label>Film Exhibited</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection