@extends('layouts.app')
@section('page_title')
    Login
@endsection
@section('content')
    <section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Login</h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">Login</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-12" align="center">
                <div class="featured-boxes">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}" id="loginform">
                    {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <div class="featured-box featured-box-primary align-left mt-xlg">
                                    <div class="box-content">
                                        <h4 class="heading-primary text-uppercase mb-md">I'm a Returning User</h4>
                                        @if(Session::has('msg'))
                                            <div class="alert alert-danger">
                                                {{ Session::get('msg') }}
                                            </div>
                                        @endif
                                        <form action="/" id="frmSignIn" method="post">
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}"></div>
                                                    <div class="col-md-12">
                                                        <label>Mobile Number</label>
                                                        <input type="text" value="" class="form-control input-lg" name="email">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}"></div>
                                                    <div class="col-md-12">
                                                        <!-- <a class="pull-right" href="#">(Lost Password?)</a> -->
                                                        <label>Password</label>
                                                        <input type="password" value="" class="form-control input-lg" name="password">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- <div class="col-md-6">
                                                    <span class="remember-box checkbox">
                                                        <label for="rememberme">
                                                            <input type="checkbox" id="rememberme" name="rememberme">Remember Me
                                                        </label>
                                                    </span>
                                                </div> -->
                                                <div class="col-md-121">
                                                    <input type="submit" value="Login" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3"></div>
                           <!--  <div class="col-sm-6">
                                <div class="featured-box featured-box-primary align-left mt-xlg">
                                    <div class="box-content">
                                        <h4 class="heading-primary text-uppercase mb-md">Register An Account</h4>
                                        <form action="/" id="frmSignUp" method="post">
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label>E-mail Address</label>
                                                        <input type="text" value="" class="form-control input-lg">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>Password</label>
                                                        <input type="password" value="" class="form-control input-lg">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Re-enter Password</label>
                                                        <input type="password" value="" class="form-control input-lg">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="submit" value="Register" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/js/validation/jquery.validate.js')}}"></script>
    <script src="{{URL::asset('public/js/validation/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/js/validation/app.js')}}"></script>
    <script>
        jQuery(document).ready(function(){
            App.init();
            FormValidation.init();
        });
    </script>
@endsection
