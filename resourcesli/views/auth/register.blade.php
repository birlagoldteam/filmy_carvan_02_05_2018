@extends('layouts.app')
@section('page_title')
    Register
@endsection
@section('content')
    <section class="page-header page-header-light page-header-more-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Register</h1>
                    <ul class="breadcrumb breadcrumb-valign-mid">
                        <li><a href="{{route('welcome')}}">Home</a></li>
                        <li class="active">Register</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <form id="registerform"  class="form-horizontal form-bordered" method="post">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="container">
            <div class="row">
                <div class="col-md-12" align="center">
                    <div class="featured-boxes">
                        <div class="row">                       
                            <div class="col-sm-2"></div> 
                            <div class="col-sm-8">
                                <div class="featured-box featured-box-primary align-left mt-xlg">
                                    <div class="box-content">
                                        <h4 class="heading-primary text-uppercase mb-md">Register An Account</h4>
                                        <form action="/" id="frmSignUp" method="post">
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>Name</label>
                                                        <input type="text" value=""  class="form-control input-lg" name="name" id="name" >
                                                    </div>
                                                     <div class="col-md-6">
                                                        <label>E-mail Address</label>
                                                        <input type="email" value="" class="form-control input-lg" name="email" id="email">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>Mobile No.</label>
                                                      
                                                        <input type="text" value="" class="form-control input-lg" name="mobile" id="mobile">
                                                    </div>
                                                     <div class="col-md-6">
                                                        <label>Aadhar No.</label>
                                                        <input type="text" value="" class="form-control input-lg" name="aadhar_number" id="aadhar_number" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>Password</label>
                                                      
                                                        <input type="password" value="" class="form-control input-lg" name="password" id="password" >
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Re-enter Password</label>
                                                       
                                                        <input type="password" value="" class="form-control input-lg" name="confirmpassword" id="confirmpassword" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary pull-right mb-xl">Register</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2"></div> 
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form> 
    <<!-- div class="modal fade" id="emailsend" tabindex="-1" role="basic" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Otp Verification</h4>
                </div>
                <div class="modal-body">
                    Please Enter The Otp on Your Mobile No.        
                    <input type="text" value="" data-msg-required="Please enter your otp." data-msg-email="Please enter a valid Otp." maxlength="100" class="form-control" name="otpffghf" id="otpffghf">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green" onclick="checkotp()">Resend Otp</button>
                    <button type="button" class="btn green" id="registeruser">Validate</button>
                </div>
            </div>
        </div>
    </div> -->


    <div class="modal fade" id="emailsend" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                    <h4 class="modal-title" id="smallModalLabel">OTP Verification</h4>
                </div>
                <div class="modal-body">
                    <form id="demo-form" class="form-horizontal mb-lg" novalidate="novalidate">
                        <div class="form-group mt-lg">
                            <div class="col-sm-12">
                                <input type="text" name="name" class="form-control" placeholder="Enter Your OTP..." name="otpffghf" id="otpffghf" required/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="registeruser">Validate</button>
                    <button type="button" class="btn btn-default" onclick="checkotp()">Resend OTP</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_level_script_bottom')
    <script src="{{URL::asset('public/js/validation/jquery.validate.js')}}"></script>
    <script src="{{URL::asset('public/js/validation/form-validation.js')}}"></script>
    <script src="{{URL::asset('public/js/validation/app.js')}}"></script>
    <script>
        jQuery(document).ready(function(){
            App.init();
            FormValidation.init();
        });
        function checkotp(){
    	 	var mobile = $('#mobile').val();        	 	 
    	 	var token = $('#token').val();
            var email = $('#email').val();
            var mobile = $('#mobile').val();
            $.ajax({
                url: "{{ route('checkotp')}}",
                data:{'mobile':mobile, '_token': token,'email':email,'mobile':mobile,},
                type: 'post',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){                        	
                    if(response['success']===false){
                        alert(response['error']);
                        return false;
                    }else{
                        $('#emailsend').modal('show');
                    }
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function (data) {
                    $('#loading').hide();
                }
            });
        }

        $('#registeruser').click(function(){
            var otp =$('#otpffghf').val();
            if(otp==""){
                alert("Please Enter Otp!!!");
                return false;
            }
            var name = $('#name').val();
    	    var email = $('#email').val();
    	 	var mobile = $('#mobile').val();
    	 	var aadhar_number = $('#aadhar_number').val();
    	 	var password = $('#password').val();
    	 	var token = $('#token').val();        	 
    	 	$.ajax({
                url: "{{ route('member-register-post')}}",
                data:{'name':name,'email':email,'mobile':mobile,'aadhar_number':aadhar_number,'password':password,'otp':otp,'_token': token},
                type: 'post',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                    if(response['success']===false){
                        alert(response['error']);                            
                    }else{
                        window.location = "{{ route('membershipregistration') }}";
                    }
                },
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
        });

    </script>
@endsection         
