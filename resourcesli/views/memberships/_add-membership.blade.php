@extends('layouts.app')
@section('page_title')
    Members
@endsection
@section('page_level_style_top')
<link rel="stylesheet" href="{{ URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}" />
@endsection
@section('content')
<div class="container">
	
	<div class="agent-item">
		<div class="row">
			<!-- <div class="col-md-2">
				<img src="img/team/team-11.jpg" class="img-responsive" alt="">
			</div> -->
			<div class="col-md-6">
				<h3>{{Auth::user()->name}}</h3>
				<!-- <h6 class="mb-xs">ssss</h6> -->
				<h5 class="mt-xs mb-xs">Click <i class="fa fa-hand-o-down"></i> for Membership Registration </h5>
				<a class="btn btn-secondary btn-sm mt-md" href="#membership">Membership Registration</a>
			</div>
			<div class="col-md-4">
				<ul class="list list-icons m-sm ml-xl">
					<li>
						<a href="mailto: mail@domain.com">
							<i class="icon-envelope-open icons"></i> {{Auth::user()->email}}
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-call-out icons"></i> {{Auth::user()->mobile}}
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-social-linkedin icons"></i> Lindekin
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-social-facebook icons"></i> Facebook
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	@if(empty($membership))
	<div id="membership" class="heading heading-border heading-middle-border heading-middle-border-center">
		<h2>Filmy Caravan <strong>Membership Registration</strong></h2>
	</div>

	<!-- <div id="membership" class="row">
		<h2 align="center">Filmy Caravan Membership Registration</h2>
	</div> -->
	
	
	<div class="row">
		<div class="col-md-12">
			<div class="agent-item agent-item-detail">
				<div class="row">
					<div class="col-md-12">
						<form id="membershipForm" name="membershipForm" action="{{ url('membership-create') }}" method="POST">
							{{ csrf_field() }}
							<input type="hidden" value="Contact Form" name="subject" id="subject">
							<input type="hidden" name="id" id="id">
							{{$membership}}
							<div class="row">
								<h3>1. Personal Info.</h3>
								<div class="form-group">
									<div class="col-md-4">
										<label>Name *</label>
										<input type="text" value="{{Auth::user()->name}}" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" readonly>
									</div>
									<div class="col-md-4">
										<label>Email *</label>
										<input type="email" value="{{Auth::user()->email}}" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" readonly>
									</div>
									<div class="col-md-4">
										<label>Mobile *</label>
										<input type="text" value="{{Auth::user()->mobile}}" data-msg-required="Please enter your Mobile." maxlength="100" class="form-control" name="mobile" id="mobile" readonly>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-4">
										<label>Gender *</label>
										<select class="form-control mb-md" name="gender" id="gender">
											<option value="">---</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
											<option value="Other">Other</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Date of Birth *</label>
										<input type="text" value="" class="form-control datepicker" name="date_of_birth" id="date_of_birth">
									</div>
									<div class="col-md-4">
										<label>Aadhar No. *</label>
										<input type="text" value="" data-msg-required="Please enter your Adhar No." maxlength="100" class="form-control" name="aadhar_number" id="aadhar">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-4">
										<label style="display:none;" class="gender_other">Gender Other</label>
										<input style="display:none;" type="text" value="" class="form-control gender_other" name="gender_other" id="gender_other">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-4">
										<label>PAN *</label>
										<input type="text" value="" data-msg-required="Please enter your PAN." maxlength="100" class="form-control" name="pan" id="pan">
									</div>
									<div class="col-md-4">

										
										<label>Marital Status *</label><br />
										<label class="checkbox-inline">
											<input type="radio" id="marital_status" name="marital_status" value="Married"> Married
										</label>
										<label class="checkbox-inline">
											<input type="radio" id="marital_status" name="marital_status" value="Unmarried"> Unmarried
										</label>
									</div>
									<div class="col-md-4">
										<label>Children *</label><br />
										<label class="checkbox-inline">
											<input type="radio" id="children" name="children" value="Yes"> Yes
										</label>
										<label class="checkbox-inline">
											<input type="radio" id="children" name="children" value="No"> No
										</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label>Blood Group *</label>
									<input type="text" value="" data-msg-required="Please enter your Blood Group." maxlength="100" class="form-control" name="blood_group" id="blood_group">
								</div>
							</div>
							<div class="row">
								<h5>Address</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Country *</label>
										<select class="form-control mb-md" name="country">
											<option value="1">India</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>State *</label>
										<select class="form-control mb-md" name="state" id="state">
											<option value="">---</option>
											@foreach($state as $value)
												<option value="{{$value->id}}">{{$value->state_name}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-4">
										<label>City/District *</label>
										<select class="form-control mb-md" name="city" id="city">
											<option value="">---</option>
										</select>
									</div>
								</div>
								<div class="form-group">	
									<div class="col-md-4">
										<label>Tehsil *</label>
										<select class="form-control mb-md" name="tehsil" id="tehsil">
											<option value="">---</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Gram Panchayat *</label>
										<select class="form-control mb-md" name="grampanchayat" id="grampanchayat">
											<option value="">---</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Village *</label>
										<input type="text" value="" data-msg-required="Please enter your Village." class="form-control" name="village" id="village">
									</div>
								</div>
								<div class="form-group">	
									<div class="col-md-4">
										<label>Stree/Area *</label>
										<input type="text" value="" data-msg-required="Please enter Street Name" maxlength="100" class="form-control" name="street_area" id="street_area">
									</div>
									<div class="col-md-4">
										<label>House Name/Number *</label>
										<input type="text" value="" data-msg-required="Please enter your Home name/Number." class="form-control" name="house_name_no" id="house_name_no">
									</div>
									<div class="col-md-4">
										<label>Pincode *</label>
										<input type="text" value="" data-msg-required="Please enter your Pincode." maxlength="100" class="form-control" name="pincode" id="pincode">
									</div>
								</div>
							</div>

							<div class="row">
								<h5>Education</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Education *</label>
										<select class="form-control mb-md" name="education" id="education">
											<option value="">---</option>
											<option value="SSC/Matriculation">SSC/Matriculation</option>
											<option value="HSC">HSC</option>
											<option value="Graduate">Graduate</option>
											<option value="Post-Graduate">Post-Graduate</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Language Known *</label>
										<input type="text" value="" data-msg-required="Please enter your Languages you Know." class="form-control" name="language_known" id="language_known">
									</div>
								</div>
							</div>

							<div class="row">
								<h5>Employment</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Employment *</label>
										<select class="form-control mb-md" name="employment" id="employment">
											<option value="">---</option>
											<option value="Working">Working</option>
											<option value="Not Working">Not Working</option>
											<option value="Retired">Retired</option>
											<option value="Other">Other</option>
										</select>
									</div>
									<div class="col-md-4 employment_other" style="display:none;">
										<label>Employment Other *</label>
										<input type="text" value="" data-msg-required="Others." maxlength="100" class="form-control" name="employment_other" id="employment_other">
									</div>
								</div>
							</div>

							<div class="row">
								<h5>Health Profile</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Handicapped *</label><br />
										<label class="checkbox-inline">
											<input type="radio" id="handicapped" name="handicapped" value="Yes"> Yes
										</label>
										<label class="checkbox-inline">
											<input type="radio" id="handicapped" name="handicapped" value="No"> No
										</label>
									</div>
									<div class="col-md-4">
										<label>Any Long-Term Diseases  *</label>
										<select class="form-control mb-md" name="long_term_diseases" id="long_term_diseases">
											<option value="">---</option>
											<option value="Polio">Polio</option>
											<option value="Tuberculosis">Tuberculosis</option>
											<option value="Other">Other</option>
											<option value="NA">NA</option>
										</select>
									</div>
									<div class="col-md-4">
	                                    <label>Social Needs for Me *</label><br />
                                        <select class="form-control multiselect" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": true }' >
										@foreach ($categories as $category)
										  <optgroup label="{{ $category->category_name }}">
										  @foreach ($category->subCategories as $need)
											
											 <option value="{{ $need->sub_category_name }}" >{{ $need->sub_category_name }}</option>
										  @endforeach
										  </optgroup>
										@endforeach
										</select>
	                                </div>
								</div>
							</div>
						
							<div class="row">
								<div class="row">
									<h3>2. Family Info.</h3>
									<div class="form-group">
										<div class="col-md-12">
											<div class="table-responsive">
												<table class="table table-bordered table-striped table-condensed mb-none">
													<thead>
														<tr>
															<th width="5%">&nbsp;</th>
															<th width="20%">Name</th>
															<th width="5%">Age</th>
															<th width="20%">Relationship</th>
															<th width="10%">Employment</th>
															<th width="20%">Mobile</th>
															<th width="20%">Social Needs</th>
														</tr>
													</thead>
													<tbody id="tbltable">
														
													</tbody>
													<tfoot>
														<tr>
															<td colspan="7"><button type="button" class="btn btn-default" onclick="add_row()">+</button></td>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>	
									</div>
								</div>
							</div>	

							<div class="row">
								<div class="row">
									<h3>3. My Financial Status</h3>
									<div class="form-group">
										<div class="col-md-4">
											<label>Family Annual Income *</label>
											<!-- <select class="form-control mb-md" name="type_of_work" id="type_of_work">
												<option value="Service">Service</option>
												<option value="Self-employed">Self-employed</option>
												<option value="PSU">PSU</option>
												<option value="Government">Government</option>
												<option value="Farming">Farming</option>
												<option value="Other">Other</option>
											</select> -->
											<input type="text" value="" data-msg-required="Please enter Street Name" maxlength="100" class="form-control other_type_of_work" name="family_annual_income" id="other_type_of_work">
										</div>
										<div class="col-md-4">
											<label>Income *</label>
											<!-- <select class="form-control mb-md" name="income" id="income">
												<option value="0K - 10K">0K - 10K</option>
												<option value="10K - 1L">10K - 1L</option>
												<option value="1L - 5L">1L - 5L</option>
												<option value="5L - and above">5L - and above</option>
												<option value="NA">NA</option>
											</select> -->
											<input type="text" value="" data-msg-required="Please enter Monthly Expenses" maxlength="100" class="form-control other_type_of_work" name="monthly_expenses" id="other_type_of_work">
										</div>
										<div class="col-md-4">
											<label>Vehicle *</label>
											<select class="form-control mb-md" name="vehicle" id="vehicle">
												<option value="Two Wheeler">Two Wheeler</option>
												<option value="Four Wheeler">Four Wheeler</option>
												<option value="Heavy Vehicle">Heavy Vehicle</option>
												<option value="Other">Other</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-4">
											<label style="display:none;" class="other_type_of_work">Other Type of Work</label>
											<input style="display:none;" type="text" value="" data-msg-required="Please enter Street Name" maxlength="100" class="form-control other_type_of_work" name="other_type_of_work" id="other_type_of_work">
										</div>
										<div class="col-md-4 col-md-offset-4">
											<label style="display:none;" class="other_vehicle">Other Vehicle</label>
											<input style="display:none;" type="text" value="" data-msg-required="Please enter Street Name" maxlength="100" class="form-control other_vehicle" name="other_vehicle" id="other_vehicle">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-4">
											<label>House *</label>
											<select class="form-control mb-md" name="house" id="house">
												<option value="Own">Own</option>
												<option value="Rented">Rented</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="col-md-4">
											<label>Loan *</label>
											<select class="form-control mb-md" name="loan" id="loan">
												<option value="Housing Loan">Housing Loan</option>
												<option value="Personal Loan">Personal Loan</option>
												<option value="Agricultural Loan">Agricultural Loan</option>
												<option value="Study Loan">Study Loan</option>
												<option value="Other">Other</option>
											</select>
										</div>
										<div class="col-md-4">
											<label>Insurance *</label>
											<select class="form-control mb-md" name="insurance" id="insurance">
												<option value="Life Insurance">Life Insurance</option>
												<option value="General Insurance">General Insurance</option>
												<option value="Health Insurance">Health Insurance</option>
												<option value="Other">Other</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-4">
											<label style="display:none;" class="other_house">Other House</label>
											<input style="display:none;" type="text" value="" data-msg-required="Please enter Other House" maxlength="100" class="form-control other_house" name="other_house" id="other_house">
										</div>
										<div class="col-md-4">
											<label style="display:none;" class="other_loan">Other Loan</label>
											<input style="display:none;" type="text" value="" data-msg-required="Please enter Other Loan" maxlength="100" class="form-control other_loan" name="other_loan" id="other_loan">
										</div>
										<div class="col-md-4">
											<label style="display:none;" class="other_insurance">Other Insurance</label>
											<input style="display:none;" type="text" value="" data-msg-required="Please enter Other Insurance" maxlength="100" class="form-control other_insurance" name="other_insurance" id="other_insurance">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="row">
									<h3>4. Social Welfare</h3>
									<div class="form-group">
										<div class="col-md-12">
											<label>Social Welfare Needs *</label></div>
											<div class="col-md-12">
												<label class="checkbox-inline">
													<input type="checkbox" id="inlineCheckbox1" value="true" name="education"> Education
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" id="inlineCheckbox2" value="true" name="water_sanitation"> Water & Sanitation
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" id="inlineCheckbox3" value="true" name="healthcare"> Healthcare
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" id="inlineCheckbox4" value="true" name="environmental"> Environmental
												</label>
												<label class="checkbox-inline">	
													<input type="checkbox" id="inlineCheckbox5" value=true name="economic_empowerment"> Social & Economic Empowerment
												<label class="checkbox-inline">
													<input type="checkbox" id="inlineCheckbox6" value="true" name="sports_culture"> Sports & Culture
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Register" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
</div>
@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/js/validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/js/validation/form-validation.js')}}"></script>
<script src="{{URL::asset('public/js/validation/app.js')}}"></script>
<script src="{{URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script>
	var index = 0;
	function needs(i) {
		$('#family_social_needs_'+i).val($('#needs'+i).val());
	}
	jQuery(document).ready(function () {
		App.init();
        FormValidation.init();
        $('.multiselect').multiselect();
		
		$('#gender').change(function() {
			if($('#gender').val() == 'Other') {
				$('.gender_other').show();
			} else {
				$('#gender_other').val('');
				$('.gender_other').hide();
			}
		});
		$('#gender').trigger('change');
		
		$('#employment').change(function() {
			if($('#employment').val() == 'Other') {
				$('.employment_other').show();
			} else {
				$('#employment_other').val('');
				$('.employment_other').hide();
			}
		});
		$('#employment').trigger('change');
		
		$('#type_of_work').change(function() {
			if($('#type_of_work').val() == 'Other') {
				$('.other_type_of_work').show();
			} else {
				$('#other_type_of_work').val('');
				$('.other_type_of_work').hide();
			}
		});
		$('#type_of_work').trigger('change');
		
		$('#vehicle').change(function() {
			if($('#vehicle').val() == 'Other') {
				$('.other_vehicle').show();
			} else {
				$('#other_vehicle').val('');
				$('.other_vehicle').hide();
			}
		});
		$('#vehicle').trigger('change');
		
		$('#house').change(function() {
			if($('#house').val() == 'Other') {
				$('.other_house').show();
			} else {
				$('#other_house').val('');
				$('.other_house').hide();
			}
		});
		$('#house').trigger('change');
		
		$('#loan').change(function() {
			if($('#loan').val() == 'Other') {
				$('.other_loan').show();
			} else {
				$('#other_loan').val('');
				$('.other_loan').hide();
			}
		});
		$('#loan').trigger('change');
		
		$('#insurance').change(function() {
			if($('#insurance').val() == 'Other') {
				$('.other_insurance').show();
			} else {
				$('#other_insurance').val('');
				$('.other_insurance').hide();
			}
		});
		$('#insurance').trigger('change');
		
	});
	
	function formsubmit(form){
		//$('#loading').show();
		form.submit();
	}
	
	function add_row() {
		var html = '';
		html += '<tr id="row'+index+'">';
			html += '<td>';
				html += '<button class="btn btn-default" type="button" onclick="remove_row('+index+')">-</button>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_name[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_age[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_relationship[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_employment[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_mobile[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<select class="form-control multiselect" multiple="multiple" data-plugin-multiselect onchange="needs('+index+')" id="needs'+index+'">';
				@foreach ($categories as $category)
					html += '<optgroup label="{{ $category->category_name }}">';
					@foreach ($category->subCategories as $need)
						html += '<option value="{{ $need->sub_category_name }}">{{ $need->sub_category_name }}</option>';
					@endforeach
					html += '</optgroup>';
				@endforeach
				html += '</select>';
				html += '<input type="hidden" class="form-control" id="family_social_needs_'+index+'" name="family_social_needs[]">';
			html += '</td>';
		html += '</tr>';
		$("#tbltable").append(html);
		$('.multiselect').multiselect();
		index++;
	}
	
	function remove_row(i) {
		var r = confirm("Do you want to remove this row ?");
		if (r == true) {
			$("#row"+i).remove();
		}
	}

	$('#state').change(function(){
		var value = $(this).val();
		$("#city option,#tehsil option,#grampanchayat option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getdistrict')}}",
                data:{'state_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#city").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#city").append(
				      		$("<option></option>").text(item.district_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});

	$('#city').change(function(){
		var value = $(this).val();
		$("#tehsil option,#grampanchayat option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('gettehsil')}}",
                data:{'district_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#tehsil").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#tehsil").append(
				      		$("<option></option>").text(item.tehsil_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});

	$('#tehsil').change(function(){
		var value = $(this).val();
		$("#grampanchayat option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getgrampanchayat')}}",
                data:{'tehsil_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#grampanchayat").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#grampanchayat").append(
				      		$("<option></option>").text(item.panchayat_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});
</script>
@endsection


