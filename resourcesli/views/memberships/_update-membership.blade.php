@extends('layouts.app')
@section('page_title')
    Members
@endsection
@section('page_level_style_top')
<link rel="stylesheet" href="{{ URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.css')}}" />
@endsection
@section('content')
<div class="container">
	
	<div class="agent-item">
		<div class="row">
			<!-- <div class="col-md-2">
				<img src="img/team/team-11.jpg" class="img-responsive" alt="">
			</div> -->
			<div class="col-md-6">
				<h3>{{Auth::user()->name}}</h3>
				<!-- <h6 class="mb-xs">ssss</h6> -->
				<h5 class="mt-xs mb-xs">Click <i class="fa fa-hand-o-down"></i> for Membership Registration </h5>
				<a class="btn btn-secondary btn-sm mt-md" href="#membership">Membership Registration</a>
			</div>
			<div class="col-md-4">
				<ul class="list list-icons m-sm ml-xl">
					<li>
						<a href="mailto: mail@domain.com">
							<i class="icon-envelope-open icons"></i> {{Auth::user()->email}}
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-call-out icons"></i> {{Auth::user()->mobile}}
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-social-linkedin icons"></i> Lindekin
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-social-facebook icons"></i> Facebook
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<div id="membership" class="heading heading-border heading-middle-border heading-middle-border-center">
		<h2>Filmy Caravan <strong>Membership Registration</strong></h2>
	</div>
	
	
	<div class="row">
		<div class="col-md-12">
			<div class="agent-item agent-item-detail">
				<div class="row">
					<div class="col-md-12">
						<form id="membershipForm" name="membershipForm" id="membershipForm" action="{{ url('membership-update') }}" method="POST">
							{{ csrf_field() }}
							<input type="hidden" value="Contact Form" name="subject" id="subject">
							<input type="hidden" value="{{$user->id}}" name="id" id="id">
							<div class="row">
								<h3>1. Personal Info.</h3>
								<div class="form-group">
									<div class="col-md-4">
										<label>Name *</label>
										<input type="text" value="{{$user->name}}" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" readonly>
									</div>
									<div class="col-md-4">
										<label>Email *</label>
										<input type="email" value="{{$user->email}}" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" readonly>
									</div>
									<div class="col-md-4">
										<label>Mobile *</label>
										<input type="text" value="{{$user->mobile}}" data-msg-required="Please enter your Mobile." maxlength="100" class="form-control" name="mobile" id="mobile" readonly>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-4">
										<label>Gender *</label>
										<select class="form-control mb-md" name="gender" id="gender">
											<option value="">---</option>
											<option value="Male" <?php if ($user->gender == 'Male') echo 'selected'; ?>>Male</option>
											<option value="Female" <?php if ($user->gender == 'Female') echo 'selected'; ?>>Female</option>
											<option value="Other" <?php if ($user->gender == 'Other') echo 'selected'; ?>>Other</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Date of Birth *</label>
										<input type="text" value="{{$user->date_of_birth}}" class="form-control datepicker" name="date_of_birth" id="date_of_birth">
									</div>
									<div class="col-md-4">
										<label>Aadhar No. *</label>
										<input type="text" value="{{$user->aadhar_number}}" data-msg-required="Please enter your Adhar No." maxlength="100" class="form-control" name="aadhar_number" id="aadhar">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-4">
										<label style="display:none;" class="gender_other">Gender Other</label>
										<input style="display:none;" type="text" value="{{$user->gender_other}}" class="form-control gender_other" name="gender_other" id="gender_other">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-4">
										<label>PAN *</label>
										<input type="text" value="{{$user->pan}}" data-msg-required="Please enter your PAN." maxlength="100" class="form-control" name="pan" id="pan">
									</div>
									<div class="col-md-4">
			<!--
										<label>Marital Status</label>
										<select class="form-control mb-md" name="marital_status">
											<option value="">---</option>
											<option value="Married" <?php if ($user->marital_status == 'Married') echo 'selected'; ?>>Married</option>
											<option value="Single" <?php if ($user->marital_status == 'Single') echo 'selected'; ?>>Single</option>
										</select>
			-->
										
										<label>Marital Status *</label><br />
										<label class="checkbox-inline">
											<input type="radio" id="marital_status" name="marital_status" value="Married" <?php if ($user->marital_status == 'Married') echo 'checked'; ?>> Married
										</label>
										<label class="checkbox-inline">
											<input type="radio" id="marital_status" name="marital_status" value="Unmarried" <?php if ($user->marital_status == 'Unmarried') echo 'checked'; ?>> Unmarried
										</label>
									</div>
									<div class="col-md-4">
			<!--
										<label>Children *</label>
										<input type="text" value="{{$user->children}}" data-msg-required="Please enter Total No. of Childrens." maxlength="100" class="form-control" name="children" id="children">
			-->
										<label>Children *</label><br />
										<label class="checkbox-inline">
											<input type="radio" id="children" name="children" value="Yes" <?php if ($user->children == 'Yes') echo 'checked'; ?>> Yes
										</label>
										<label class="checkbox-inline">
											<input type="radio" id="children" name="children" value="No" <?php if ($user->children == 'No') echo 'checked'; ?>> No
										</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label>Blood Group *</label>
									<input type="text" value="{{$user->blood_group}}" data-msg-required="Please enter your Blood Group." maxlength="100" class="form-control" name="blood_group" id="blood_group">
								</div>
							</div>
							<div class="row">
								<h5>Address</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Country *</label>
										<select class="form-control mb-md" name="country">
											<option value="1" <?php if ($user->country == '1') echo 'selected'; ?>>India</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>State *</label>
										<select class="form-control mb-md" name="state" id="state">
											<option value="">---</option>
											@foreach($state as $value)
												<option value="{{$value->id}}" <?php if ($user->state == $value->id) echo 'selected'; ?>>{{$value->state_name}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-4">
										<label>City/District *</label>
										<select class="form-control mb-md" name="city" id="city">
											<option value="">---</option>
										</select>
									</div>
								</div>
								<div class="form-group">	
									<div class="col-md-4">
										<label>Tehsil *</label>
										<select class="form-control mb-md" name="tehsil" id="tehsil">
											<option value="">---</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Gram Panchayat *</label>
										<select class="form-control mb-md" name="grampanchayat" id="grampanchayat">
											<option value="">---</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Village *</label>
										<input type="text" value="{{$user->village}}" data-msg-required="Please enter your Village." class="form-control" name="village" id="village">
									</div>
								</div>
								<div class="form-group">	
									<div class="col-md-4">
										<label>Stree/Area *</label>
										<input type="text" value="{{$user->street_area}}" data-msg-required="Please enter Street Name" maxlength="100" class="form-control" name="street_area" id="street_area">
									</div>
									<div class="col-md-4">
										<label>House Name/Number *</label>
										<input type="text" value="{{$user->house_name_no}}" data-msg-required="Please enter your Home name/Number." class="form-control" name="house_name_no" id="house_name_no">
									</div>
									<div class="col-md-4">
										<label>Pincode *</label>
										<input type="text" value="{{$user->pincode}}" data-msg-required="Please enter your Pincode." maxlength="100" class="form-control" name="pincode" id="pincode">
									</div>
								</div>
							</div>

							<div class="row">
								<h5>Education</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Education *</label>
										<select class="form-control mb-md" name="education" id="education">
											<option value="">---</option>
											<option value="SSC/Matriculation" <?php if ($user->education == 'SSC/Matriculation') echo 'selected'; ?>>SSC/Matriculation</option>
											<option value="HSC" <?php if ($user->education == 'HSC') echo 'selected'; ?>>HSC</option>
											<option value="Graduate" <?php if ($user->education == 'Graduate') echo 'selected'; ?>>Graduate</option>
											<option value="Post-Graduate" <?php if ($user->education == 'Post-Graduate') echo 'selected'; ?>>Post-Graduate</option>
										</select>
									</div>
									<div class="col-md-4">
										<label>Language Known *</label>
										<input type="text" value="{{$user->language_known}}" data-msg-required="Please enter your Languages you Know." class="form-control" name="language_known" id="language_known">
									</div>
								</div>
							</div>

							<div class="row">
								<h5>Employment</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Employment *</label>
										<select class="form-control mb-md" name="employment" id="employment">
											<option value="">---</option>
											<option value="Working" <?php if ($user->employment == 'Working') echo 'selected'; ?>>Working</option>
											<option value="Not Working" <?php if ($user->employment == 'Not Working') echo 'selected'; ?>>Not Working</option>
											<option value="Retired" <?php if ($user->employment == 'Retired') echo 'selected'; ?>>Retired</option>
											<option value="Other" <?php if ($user->employment == 'Other') echo 'selected'; ?>>Other</option>
										</select>
									</div>
									<div class="col-md-4 employment_other" style="display:none;">
										<label>Employment Other *</label>
										<input type="text" value="{{$user->employment_other}}" data-msg-required="Others." maxlength="100" class="form-control" name="employment_other" id="employment_other">
									</div>
								</div>
							</div>

							<div class="row">
								<h5>Health Profile</h5>
								<div class="form-group">
									<div class="col-md-4">
										<label>Handicapped *</label><br />
										<label class="checkbox-inline">
											<input type="radio" id="handicapped" name="handicapped" value="Yes" <?php if ($user->handicapped == 'Yes') echo 'checked'; ?>> Yes
										</label>
										<label class="checkbox-inline">
											<input type="radio" id="handicapped" name="handicapped" value="No" <?php if ($user->handicapped == 'No') echo 'checked'; ?>> No
										</label>
			<!--
										<input type="text" value="{{$user->handicapped}}" class="form-control" name="handicapped" id="handicapped">
			-->
									</div>
									<div class="col-md-4">
										<label>Any Long-Term Diseases  *</label>
										<select class="form-control mb-md" name="long_term_diseases" id="long_term_diseases">
											<option value="">---</option>
											<option value="Polio" <?php if ($user->long_term_diseases == 'Polio') echo 'selected'; ?>>Polio</option>
											<option value="Tuberculosis" <?php if ($user->long_term_diseases == 'Tuberculosis') echo 'selected'; ?>>Tuberculosis</option>
											<option value="Other" <?php if ($user->long_term_diseases == 'Other') echo 'selected'; ?>>Other</option>
											<option value="NA" <?php if ($user->long_term_diseases == 'NA') echo 'selected'; ?>>NA</option>
										</select>
			<!--
										<input type="text" value="{{$user->long_term_diseases}}" class="form-control" name="long_term_diseases" id="long_term_diseases">
			-->
									</div>
								</div>
							</div>
						
							<div class="row">
								<div class="row">
									<h3>2. Family Info.</h3>
									<div class="form-group">
										<div class="col-md-12">
											<div class="table-responsive">
												<table class="table table-bordered table-striped table-condensed mb-none">
													<thead>
														<tr>
															<th width="5%">&nbsp;</th>
															<th width="20%">Name</th>
															<th width="5%">Age</th>
															<th width="20%">Relationship</th>
															<th width="10%">Employment</th>
															<th width="20%">Mobile</th>
															<th width="20%">Social Needs</th>
														</tr>
													</thead>
													<tbody id="tbltable">
														
														@if(!empty($user->familyMembers))
															@foreach($user->familyMembers as $key=>$family)
																<tr id="row{{$key}}">
																	<td><button class="btn btn-default" type="button" onclick="remove_row({{$key}})">-</button></td>
																	<td>
																		<input type="text" value="{{$family->name}}" class="form-control" name="family_name[]">
																	</td>
																	<td><input type="text" value="{{$family->age}}" class="form-control" name="family_age[]"></td>
																	<td><input type="text" value="{{$family->relationship}}" class="form-control" name="family_relationship[]"></td>
																	<td><input type="text" value="{{$family->employment}}" class="form-control" name="family_employment[]"></td>
																	<td><input type="text" value="{{$family->mobile}}" class="form-control" name="family_mobile[]"></td>
																	<td>
																		<select class="form-control multiselect" multiple="multiple" data-plugin-multiselect data-plugin-options='{ "includeSelectAllOption": true }' onchange="needs({{$key}})" id="needs{{$key}}">
																		@foreach ($categories as $category)
																		  <optgroup label="{{ $category->category_name }}">
																		  @foreach ($category->subCategories as $need)
																			<?php
																				$selected = '';
																				$array = explode(',',$family->social_needs);
																				if(in_array($need->sub_category_name, $array)) {
																					$selected = 'selected';
																				}
																			?>
																			 <option value="{{ $need->sub_category_name }}" {{ $selected }}>{{ $need->sub_category_name }}</option>
																		  @endforeach
																		  </optgroup>
																		@endforeach
																		</select>
																		<input type="hidden" value="{{$family->social_needs}}" class="form-control" id="family_social_needs_{{$key}}" name="family_social_needs[]">
																	</td>
																</tr>
															@endforeach
														@endif
													</tbody>
													<tfoot>
														<tr>
															<td colspan="7"><button type="button" class="btn btn-default" onclick="add_row()">+</button></td>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>	
									</div>
								</div>
							</div>	

							<div class="row">
								<div class="row">
									<h3>3. Income Info.</h3>
									<div class="form-group">
										<div class="col-md-4">
											<label>Type of Work *</label>
											<select class="form-control mb-md" name="type_of_work" id="type_of_work">
												<option value="Service" <?php if ($user->type_of_work == 'Service') echo 'selected'; ?>>Service</option>
												<option value="Self-employed" <?php if ($user->type_of_work == 'Self-employed') echo 'selected'; ?>>Self-employed</option>
												<option value="PSU" <?php if ($user->type_of_work == 'PSU') echo 'selected'; ?>>PSU</option>
												<option value="Government" <?php if ($user->type_of_work == 'Government') echo 'selected'; ?>>Government</option>
												<option value="Farming" <?php if ($user->type_of_work == 'Farming') echo 'selected'; ?>>Farming</option>
												<option value="Other" <?php if ($user->type_of_work == 'Other') echo 'selected'; ?>>Other</option>
											</select>
										</div>
										<div class="col-md-4">
											<label>Income *</label>
											<select class="form-control mb-md" name="income" id="income">
												<option value="0K - 10K" <?php if ($user->income == '0K - 10K') echo 'selected'; ?>>0K - 10K</option>
												<option value="10K - 1L" <?php if ($user->income == '10K - 1L') echo 'selected'; ?>>10K - 1L</option>
												<option value="1L - 5L" <?php if ($user->income == '1L - 5L') echo 'selected'; ?>>1L - 5L</option>
												<option value="5L - and above" <?php if ($user->income == '5L - and above') echo 'selected'; ?>>5L - and above</option>
												<option value="NA" <?php if ($user->income == 'NA') echo 'selected'; ?>>NA</option>
											</select>
										</div>
										<div class="col-md-4">
											<label>Vehicle *</label>
											<select class="form-control mb-md" name="vehicle" id="vehicle">
												<option value="Two Wheeler" <?php if ($user->vehicle == 'Two Wheeler') echo 'selected'; ?>>Two Wheeler</option>
												<option value="Four Wheeler" <?php if ($user->vehicle == 'Four Wheeler') echo 'selected'; ?>>Four Wheeler</option>
												<option value="Heavy Vehicle" <?php if ($user->vehicle == 'Heavy Vehicle') echo 'selected'; ?>>Heavy Vehicle</option>
												<option value="Other" <?php if ($user->vehicle == 'Other') echo 'selected'; ?>>Other</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-4">
											<label style="display:none;" class="other_type_of_work">Other Type of Work</label>
											<input style="display:none;" type="text" value="{{$user->other_type_of_work}}" data-msg-required="Please enter Street Name" maxlength="100" class="form-control other_type_of_work" name="other_type_of_work" id="other_type_of_work">
										</div>
										<div class="col-md-4 col-md-offset-4">
											<label style="display:none;" class="other_vehicle">Other Vehicle</label>
											<input style="display:none;" type="text" value="{{$user->other_vehicle}}" data-msg-required="Please enter Street Name" maxlength="100" class="form-control other_vehicle" name="other_vehicle" id="other_vehicle">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-4">
											<label>House *</label>
											<select class="form-control mb-md" name="house" id="house">
												<option value="Own" <?php if ($user->house == 'Own') echo 'selected'; ?>>Own</option>
												<option value="Rented" <?php if ($user->house == 'Rented') echo 'selected'; ?>>Rented</option>
												<option value="Other" <?php if ($user->house == 'Other') echo 'selected'; ?>>Other</option>
											</select>
										</div>
										<div class="col-md-4">
											<label>Loan *</label>
											<select class="form-control mb-md" name="loan" id="loan">
												<option value="Housing Loan" <?php if ($user->loan == 'Housing Loan') echo 'selected'; ?>>Housing Loan</option>
												<option value="Personal Loan" <?php if ($user->loan == 'Personal Loan') echo 'selected'; ?>>Personal Loan</option>
												<option value="Agricultural Loan" <?php if ($user->loan == 'Agricultural Loan') echo 'selected'; ?>>Agricultural Loan</option>
												<option value="Study Loan" <?php if ($user->loan == 'Study Loan') echo 'selected'; ?>>Study Loan</option>
												<option value="Other" <?php if ($user->loan == 'Other') echo 'selected'; ?>>Other</option>
											</select>
										</div>
										<div class="col-md-4">
											<label>Insurance *</label>
											<select class="form-control mb-md" name="insurance" id="insurance">
												<option value="Life Insurance" <?php if ($user->insurance == 'Life Insurance') echo 'selected'; ?>>Life Insurance</option>
												<option value="General Insurance" <?php if ($user->insurance == 'General Insurance') echo 'selected'; ?>>General Insurance</option>
												<option value="Health Insurance" <?php if ($user->insurance == 'Health Insurance') echo 'selected'; ?>>Health Insurance</option>
												<option value="Other" <?php if ($user->insurance == 'Other') echo 'selected'; ?>>Other</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="form-group">
										<div class="col-md-4">
											<label style="display:none;" class="other_house">Other House</label>
											<input style="display:none;" type="text" value="{{$user->other_house}}" data-msg-required="Please enter Other House" maxlength="100" class="form-control other_house" name="other_house" id="other_house">
										</div>
										<div class="col-md-4">
											<label style="display:none;" class="other_loan">Other Loan</label>
											<input style="display:none;" type="text" value="{{$user->other_loan}}" data-msg-required="Please enter Other Loan" maxlength="100" class="form-control other_loan" name="other_loan" id="other_loan">
										</div>
										<div class="col-md-4">
											<label style="display:none;" class="other_insurance">Other Insurance</label>
											<input style="display:none;" type="text" value="{{$user->other_insurance}}" data-msg-required="Please enter Other Insurance" maxlength="100" class="form-control other_insurance" name="other_insurance" id="other_insurance">
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="row">
									<h3>4. Social Welfare</h3>
									<div class="form-group">
										<div class="col-md-12">
											<label>Social Welfare Needs *</label></div>
											<div class="col-md-12">
												<label class="checkbox-inline">
													<input type="checkbox" id="inlineCheckbox1" value="true" name="education" <?php if ($user->social_education == true) echo 'checked'; ?>> Education
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" id="inlineCheckbox2" value="true" name="water_sanitation" <?php if ($user->water_and_sanitation == true) echo 'checked'; ?>> Water & Sanitation
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" id="inlineCheckbox3" value="true" name="healthcare" <?php if ($user->healthcare == true) echo 'checked'; ?>> Healthcare
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" id="inlineCheckbox4" value="true" name="environmental" <?php if ($user->environmental == true) echo 'checked'; ?>> Environmental
												</label>
												<label class="checkbox-inline">	
													<input type="checkbox" id="inlineCheckbox5" value=true name="economic_empowerment" <?php if ($user->social_economic_empowerment == true) echo 'checked'; ?>> Social & Economic Empowerment
												<label class="checkbox-inline">
													<input type="checkbox" id="inlineCheckbox6" value="true" name="sports_culture" <?php if ($user->sports_and_culture == true) echo 'checked'; ?>> Sports & Culture
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						
							<div class="row">
								<div class="col-md-12">
									<input type="submit" value="Register" class="btn btn-secondary mb-xlg" data-loading-text="Loading...">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('page_level_script_bottom')
<script src="{{URL::asset('public/js/validation/jquery.validate.js')}}"></script>
<script src="{{URL::asset('public/js/validation/form-validation.js')}}"></script>
<script src="{{URL::asset('public/js/validation/app.js')}}"></script>
<script src="{{URL::asset('public/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
<script>
	var index = {{count($user->familyMembers)}};
	function needs(i) {
		$('#family_social_needs_'+i).val($('#needs'+i).val());
	}
	jQuery(document).ready(function(){
        App.init();
        FormValidation.init();
        $('.multiselect').multiselect();
        
		$('#gender').change(function() {
			if($('#gender').val() == 'Other') {
				$('.gender_other').show();
			} else {
				$('#gender_other').val('');
				$('.gender_other').hide();
			}
		});
		$('#gender').trigger('change');
		
		$('#employment').change(function() {
			if($('#employment').val() == 'Other') {
				$('.employment_other').show();
			} else {
				$('#employment_other').val('');
				$('.employment_other').hide();
			}
		});
		$('#employment').trigger('change');
		
		$('#type_of_work').change(function() {
			if($('#type_of_work').val() == 'Other') {
				$('.other_type_of_work').show();
			} else {
				$('#other_type_of_work').val('');
				$('.other_type_of_work').hide();
			}
		});
		$('#type_of_work').trigger('change');
		
		$('#vehicle').change(function() {
			if($('#vehicle').val() == 'Other') {
				$('.other_vehicle').show();
			} else {
				$('#other_vehicle').val('');
				$('.other_vehicle').hide();
			}
		});
		$('#vehicle').trigger('change');
		
		$('#house').change(function() {
			if($('#house').val() == 'Other') {
				$('.other_house').show();
			} else {
				$('#other_house').val('');
				$('.other_house').hide();
			}
		});
		$('#house').trigger('change');
		
		$('#loan').change(function() {
			if($('#loan').val() == 'Other') {
				$('.other_loan').show();
			} else {
				$('#other_loan').val('');
				$('.other_loan').hide();
			}
		});
		$('#loan').trigger('change');
		
		$('#insurance').change(function() {
			if($('#insurance').val() == 'Other') {
				$('.other_insurance').show();
			} else {
				$('#other_insurance').val('');
				$('.other_insurance').hide();
			}
		});
		$('#insurance').trigger('change');
		
	});
	
	function formsubmit(form){
		//$('#loading').show();
		form.submit();
	}
	
	function add_row() {
		var html = '';
		html += '<tr id="row'+index+'">';
			html += '<td>';
				html += '<button class="btn btn-default" type="button" onclick="remove_row('+index+')">-</button>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_name[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_age[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_relationship[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_employment[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<input type="text" value="" class="form-control" name="family_mobile[]" required>';
			html += '</td>';
			html += '<td>';
				html += '<select class="form-control multiselect" multiple="multiple" data-plugin-multiselect onchange="needs('+index+')" id="needs'+index+'">';
				@foreach ($categories as $category)
					html += '<optgroup label="{{ $category->category_name }}">';
					@foreach ($category->subCategories as $need)
						html += '<option value="{{ $need->sub_category_name }}">{{ $need->sub_category_name }}</option>';
					@endforeach
					html += '</optgroup>';
				@endforeach
				html += '</select>';
				html += '<input type="hidden" class="form-control" id="family_social_needs_'+index+'" name="family_social_needs[]">';
			html += '</td>';
		html += '</tr>';
		$("#tbltable").append(html);
		$('.multiselect').multiselect();
		index++;
	}
	
	function remove_row(i) {
		var r = confirm("Do you want to remove this row ?");
		if (r == true) {
			$("#row"+i).remove();
		}
	}

	$('#state').change(function(){
		var value = $(this).val();
		$("#city option,#tehsil option,#grampanchayat option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getdistrict')}}",
                data:{'state_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#city").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
                   		var selected = '';
                   		var city = {{$user->city}}};
                   		if(item.id == city) {
                   			selected = 'selected';
                   		}
                   		alert(selected);
				      	$("#city").append(
				      		$("<option></option>").text(item.district_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});

	$('#city').change(function(){
		var value = $(this).val();
		$("#tehsil option,#grampanchayat option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('gettehsil')}}",
                data:{'district_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#tehsil").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#tehsil").append(
				      		$("<option></option>").text(item.tehsil_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});

	$('#tehsil').change(function(){
		var value = $(this).val();
		$("#grampanchayat option").remove();
		if(value!=""){
			$.ajax({
                url: "{{ route('getgrampanchayat')}}",
                data:{'tehsil_id':value},
                type: 'get',
                cache: false,
                clearForm: false,
                beforeSend:function(){  
                    $('#loading').show();
                },
                success: function(response){
                	$("#grampanchayat").append($("<option></option>").text('---').val(''));
                   	$.each(response,function(index,item){
				      	$("#grampanchayat").append(
				      		$("<option></option>").text(item.panchayat_name).val(item.id)
				       	);
				    });
				},
                error:function(){
                    alert("Server is Busy!!");
                },
                complete:function(data){
                    $('#loading').hide();
                }
            });
		}
	});
</script>
@endsection
